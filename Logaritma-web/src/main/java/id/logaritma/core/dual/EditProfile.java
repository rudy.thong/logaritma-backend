/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.dual;

import id.logaritma.core.guest.ForgotPassword;
import id.logaritma.core.util.Constants;
import id.logaritma.core.util.RC;
import id.logaritma.core.util.Util;
import id.logaritma.core.util.ValidationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Accounts;
import models.EmailNotifications;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.json.JSONException;
import org.json.JSONObject;
import sessionbeans.AccountsFacadeLocal;
import sessionbeans.ConfigFacadeLocal;
import sessionbeans.EmailNotificationsFacadeLocal;

/**
 *
 * @author Herianto
 */
@WebServlet(name = "EditProfile", urlPatterns = {"/dual/edit_profile"})
public class EditProfile extends HttpServlet {
    @EJB
    private ConfigFacadeLocal configFacade;

    @EJB
    private EmailNotificationsFacadeLocal emailNotificationsFacade;
    
    @EJB
    private AccountsFacadeLocal accountsFacade;
     
    private static final Logger log = Logger.getLogger(EditProfile.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String sessionId = request.getSession().getId();
        MDC.put("sessionId", sessionId);
        log.info("EDIT PROFILE START");
        String responseCode = RC.ERROR;
        String responseMessage = RC.ERROR_MESSAGE;
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            try{
                String username = request.getParameter("username");
                String phone = request.getParameter("phone");
                String name = request.getParameter("name");
                String email = request.getParameter("email");
                String image_path = request.getParameter("image_path");
                log.info("Phone = " + phone);
                log.info("Name = " + name);
                log.info("Email = " + email);
                log.info("Image Path = " + image_path);
                Accounts account = accountsFacade.findByUsername(username);
                if(account == null) throw new ValidationException(RC.LOGIN_FAILED, RC.LOGIN_FAILED_MESSAGE);
                else{
                    account.setPhone(phone);
                    account.setEmail(email);
                    account.setName(name);
                    account.setImagePath(image_path);
                    accountsFacade.edit(account);
                    responseCode = RC.SUCCESS;
                    responseMessage = RC.SUCCESS_MSG;
                }
                JSONObject json = new JSONObject();
                json.put("status", responseCode);
                json.put("message", responseMessage);
                JSONObject data = new JSONObject();
                data.put("email", email);
                json.put("data", data);
                out.print(json.toString(Constants.JSON_INDENT_FACTOR));
            }
            catch (ValidationException ve) {
                responseCode = ve.getCode();
                responseMessage = ve.getMessage();
                log.error("VALIDATION EXCEPTION = " + ve.getMessage());
            }
            catch (NumberFormatException | JSONException e) {
                log.error("UNEXPECTED EXCEPTION = " + e.getMessage());
                JSONObject json = new JSONObject();
                json.put("status", responseCode);
                json.put("message", responseMessage);
                out.print(json.toString(Constants.JSON_INDENT_FACTOR));
            } finally {
                out.close();                
            }
        }
        log.info("EDIT PROFILE END");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
