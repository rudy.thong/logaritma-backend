/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.shipper;

import id.logaritma.core.util.Constants;
import id.logaritma.core.util.RC;
import id.logaritma.core.util.ResponseClass;
import id.logaritma.core.util.ValidationException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Accounts;
import models.Carriers;
import models.Shippers;
import org.apache.log4j.Logger;
import sessionbeans.AccountsFacadeLocal;
import sessionbeans.CarriersFacadeLocal;
import sessionbeans.ShippersFacadeLocal;

/**
 *
 * @author rudythong
 */
@WebServlet(name = "UploadShipperDocument", urlPatterns = {"/shipper/upload_shipper_document"})
public class UploadShipperDocument extends HttpServlet {

    @EJB
    private ShippersFacadeLocal shippersFacade;

    @EJB
    private AccountsFacadeLocal accountsFacade;


    private static final Logger log = Logger.getLogger(UploadShipperDocument.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ResponseClass rc = new ResponseClass(log, request, response);
        try (PrintWriter out = response.getWriter()) {
            try {                
                String sessionId;
                Long shipperId;
                String documentPath;
                Character documentType;
                try {
                    sessionId = request.getParameter("session_id");
                    shipperId = Long.valueOf(request.getParameter("shipper_id"));
                    documentPath = request.getParameter("document_path");
                    documentType = request.getParameter("document_type").charAt(0);
                } catch (NumberFormatException e ) {
                    throw new ValidationException(RC.INPUT_NOT_VALID, RC.INPUT_NOT_VALID_MSG, rc);
                }
                
                //get Shippers based on shipperId
                Shippers shipper = shippersFacade.find(shipperId);
                if(shipper == null) {
                    throw new ValidationException(RC.SHIPPER_NOT_FOUND, RC.SHIPPER_NOT_FOUND_MESSAGE, rc);
                }

                //validate sessionId and carrierId
                Accounts account = accountsFacade.findByLoginShipperId(shipper, sessionId);
                if(account == null) {
                    throw new ValidationException(RC.SHIPPER_NOT_LOGIN, RC.SHIPPER_NOT_LOGIN_MESSAGE, rc);
                }

                //update shipper
                if(documentType.equals(Constants.SHIPPER_DOCUMENT_AKTA)) shipper.setDocAktaPath(documentPath);
                if(documentType.equals(Constants.SHIPPER_DOCUMENT_NPWP)) shipper.setDocNpwpPath(documentPath);
                if(documentType.equals(Constants.SHIPPER_DOCUMENT_DOMISILI)) shipper.setDocDomisiliPath(documentPath);
                if(documentType.equals(Constants.SHIPPER_DOCUMENT_SIUP)) shipper.setDocSiupPath(documentPath);
                                        
                shippersFacade.edit(shipper);

                rc.setSuccess();
                rc.print(out);
                
            } catch (ValidationException ve) {
                ve.getResponse().print(out);
            }
        }        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
