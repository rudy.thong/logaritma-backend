/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.util;

/**
 *
 * @author rudythong
 */
public class ValidationException extends Exception {
    
    private final String status;
    private final String message;
    private final ResponseClass responseClass;
    
    public ValidationException(String status, String message) {
        this.status = status;
        this.message = message;
        this.responseClass = null;
    }
    
    public ValidationException(String status, String message, ResponseClass responseClass) {
        this.status = status;
        this.message = message;
        this.responseClass = responseClass;
        this.responseClass.setStatus(status);
        this.responseClass.setMessage(message);
    }
            
    @Override
    public String getMessage() {
        return message;
    }
    
    public String getCode() {
        return status;
    }

    public ResponseClass getResponse() {
        return responseClass;
    }
    
}