/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.util;

import java.util.Random;
import org.apache.commons.lang.RandomStringUtils;

/**
 *
 * @author Herianto
 */
public class Util {
    public static String getRandomPass(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }
}
