/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.util;

import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.json.JSONObject;

/**
 *
 * @author rudythong
 */
public class ResponseClass {
    
    private String status;
    private String message;
    private String data;
    private final Logger log;
    private final Date start;
    
    public ResponseClass(Logger log, HttpServletRequest request, HttpServletResponse response) {
        this.status = RC.ERROR;
        this.message = RC.ERROR_MESSAGE;
        this.log = log;
        this.start = new Date();
        log.info(log.getName() + " START");
        MDC.put("sessionId", request.getSession().getId());
        response.setContentType("application/json");
    }
        
    public String toJson() {
        JSONObject json = new JSONObject();
        json.put("status", status);
        json.put("message", message);
        if(status.equalsIgnoreCase(RC.SUCCESS)) log.error(json.toString());
        if(data != null) json.put("data", data);
        return json.toString(Constants.JSON_INDENT_FACTOR);        
    }    
    
    public void print(PrintWriter pw) {
        Date end = new Date();
        Long delta = end.getTime() - start.getTime();
        log.info(log.getName() + " END within " + delta +  " ms and Result " + this.toJson().replaceAll("\n", ""));
        pw.print(this.toJson());
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(String data) {
        this.data = data;
    }
    
    public void setSuccess() {
        this.status = RC.SUCCESS;
        this.message = RC.SUCCESS_MSG;
    }
}
