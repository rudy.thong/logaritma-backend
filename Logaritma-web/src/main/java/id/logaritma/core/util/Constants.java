/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.util;

/**
 *
 * @author Herianto
 */
public class Constants {
    public static final Integer JSON_INDENT_FACTOR = 3;
    
    public static final Character JOB_STATUS_NEW = 'N';
    public static final Character JOB_STATUS_NOT_MATCHED = 'M';
    public static final Character JOB_STATUS_ACTIVE = 'A';
    public static final Character JOB_STATUS_DONE = 'S';
    
    public static final Character DRIVER_STATUS_AVAILABLE = 'A';
    public static final Character VEHICLE_STATUS_AVAILABLE = 'A';

    public static final Character EMAIL_SCHEDULER_PENDING = 'P';
    
    public static final String DATE_FORMAT = "yyyyMMddHHmmss";
    public static final String DATE_BIRTH_FORMAT = "yyyyMMdd";
    
    public static final Character CARRIER_DOCUMENT_AKTA = 'A';
    public static final Character CARRIER_DOCUMENT_DOMISILI = 'D';
    public static final Character CARRIER_DOCUMENT_SIUP = 'S';
    public static final Character CARRIER_DOCUMENT_HO = 'H';
    public static final Character CARRIER_DOCUMENT_SIUT = 'T';
    public static final Character CARRIER_DOCUMENT_IUJPT = 'I';
    
    public static final Character SHIPPER_DOCUMENT_AKTA = 'A';
    public static final Character SHIPPER_DOCUMENT_NPWP = 'N';
    public static final Character SHIPPER_DOCUMENT_DOMISILI = 'D';
    public static final Character SHIPPER_DOCUMENT_SIUP = 'S';
    
    public static final Character DRIVER_DOCUMENT_SIM = 'S';
    
    public static final Character VEHICLE_DOCUMENT_STNK = 'S';
    public static final Character VEHICLE_DOCUMENT_KIR = 'K';
    public static final Character VEHICLE_DOCUMENT_TRAYEK = 'T';
    
}
