/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.util;

/**
 *
 * @author Herianto
 */
public class RC {
    public static final String SUCCESS = "0000";
    public static final String SUCCESS_MSG = "SUCCESS";
    
    public static final String INPUT_NOT_VALID = "0001";
    public static final String INPUT_NOT_VALID_MSG = "INPUT NOT VALID";

    public static final String LOGIN_FAILED = "0011";
    public static final String LOGIN_FAILED_MESSAGE = "Account Tidak Ditemukan";
    
    public static final String CITY_NOT_FOUND = "0021";
    public static final String CITY_NOT_FOUND_MESSAGE = "Kota tidak terdafftar";
    
    public static final String PROVINCE_NOT_FOUND = "0022";
    public static final String PROVINCE_NOT_FOUND_MESSAGE = "Provinsi tidak terdafftar";

    public static final String COUNTRY_NOT_FOUND = "0023";
    public static final String COUNTRY_NOT_FOUND_MESSAGE = "Negara tidak terdafftar";
    
    public static final String DRIVER_NOT_FOUND = "1021";
    public static final String DRIVER_NOT_FOUND_MESSAGE = "Driver tidak ditemukan";
    
    public static final String VEHICLE_NOT_FOUND = "1022";
    public static final String VEHICLE_NOT_FOUND_MESSAGE = "Vehicle tidak ditemukan";

    public static final String DRIVER_CARRIER_NOT_MATCH  = "1023";
    public static final String DRIVER_CARRIER_NOT_MATCH_MESSAGE = "Driver tidak dimiliki oleh Carrier ini";
    
    public static final String VEHICLE_CARRIER_NOT_MATCH  = "1024";
    public static final String VEHICLE_CARRIER_NOT_MATCH_MESSAGE = "Vehicle tidak dimiliki oleh Carrier ini";
    
    public static final String CARRIER_NOT_FOUND = "1031";
    public static final String CARRIER_NOT_FOUND_MESSAGE = "Carrier tidak ditemukan";

    public static final String CARRIER_NOT_LOGIN = "1032";
    public static final String CARRIER_NOT_LOGIN_MESSAGE = "Carrier belum login";
    
    public static final String VEHICLE_CATEGORY_NOT_FOUND = "1041";
    public static final String VEHICLE_CATEGORY_NOT_FOUND_MESSAGE = "Vehicle Category tidak ditemukan";
    
    public static final String ITEM_CATEGORY_NOT_FOUND = "1042";
    public static final String ITEM_CATEGORY_NOT_FOUND_MESSAGE = "Item Category tidak ditemukan";    

    public static final String BANK_NOT_FOUND = "1051";
    public static final String BANK_NOT_FOUND_MESSAGE = "Bank tidak ditemukan";
    
    public static final String JOB_START_DATE_NOT_VALID = "1101";
    public static final String JOB_START_DATE_NOT_VALID_MESSAGE = "Format tanggal mulai mencari pekerjaan tidak valid";
    
    public static final String JOB_END_DATE_NOT_VALID = "1101";
    public static final String JOB_END_DATE_NOT_VALID_MESSAGE = "Format tanggal selesai mencari pekerjaan tidak valid";
    
    public static final String REGISTRATION_EMAIL_EXISTED = "0012";
    public static final String REGISTRATION_EMAIL_EXISTED_MESSAGE = "Email sudah pernah terdaftar";

    public static final String SHIPPER_NOT_FOUND = "2031";
    public static final String SHIPPER_NOT_FOUND_MESSAGE = "Shipper tidak ditemukan";

    public static final String SHIPPER_NOT_LOGIN = "2032";
    public static final String SHIPPER_NOT_LOGIN_MESSAGE = "Shipper belum login";

    public static final String ERROR = "9999";
    public static final String ERROR_MESSAGE = "GENERAL ERROR";
}
