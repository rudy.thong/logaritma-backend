/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.carrier;

import id.logaritma.core.util.Constants;
import id.logaritma.core.util.RC;
import id.logaritma.core.util.ResponseClass;
import id.logaritma.core.util.ValidationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Accounts;
import models.Carriers;
import models.Cities;
import models.Drivers;
import models.ItemCategories;
import models.Jobs;
import models.VehicleCategories;
import models.Vehicles;
import org.apache.log4j.Logger;
import sessionbeans.AccountsFacadeLocal;
import sessionbeans.CarriersFacadeLocal;
import sessionbeans.ItemCategoriesFacadeLocal;
import sessionbeans.VehicleCategoriesFacadeLocal;
import sessionbeans.VehiclesFacadeLocal;

/**
 *
 * @author rudythong
 */
@WebServlet(name = "SubmitVehicle", urlPatterns = {"/carrier/submit_vehicle"})
public class SubmitVehicle extends HttpServlet {

    @EJB
    private VehiclesFacadeLocal vehiclesFacade;

    @EJB
    private AccountsFacadeLocal accountsFacade;

    @EJB
    private ItemCategoriesFacadeLocal itemCategoriesFacade1;

    @EJB
    private ItemCategoriesFacadeLocal itemCategoriesFacade;

    @EJB
    private VehicleCategoriesFacadeLocal vehicleCategoriesFacade;

    @EJB
    private CarriersFacadeLocal carriersFacade;

    private static final Logger log = Logger.getLogger(SubmitVehicle.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ResponseClass rc = new ResponseClass(log, request, response);
        try (PrintWriter out = response.getWriter()) {
            try {                
                String sessionId;
                Long carrierId;
                Long vehicleCategoryId;
                Long itemCategoryId;
                String platNumber;
                List<Long> accessoriesIds = new LinkedList<>();
                try {
                    sessionId = request.getParameter("session_id");
                    carrierId = Long.valueOf(request.getParameter("carrier_id"));
                    vehicleCategoryId = Long.valueOf(request.getParameter("vehicle_category_id"));
                    itemCategoryId = Long.valueOf(request.getParameter("item_category_id"));
                    platNumber = request.getParameter("plat_number");    
                    String[] accessoriesIdsTemp = request.getParameter("accessories_ids").split(";");
                    for(int i = 0; i < accessoriesIdsTemp.length; ++i) {
                        accessoriesIds.add(Long.valueOf(accessoriesIdsTemp[i]));
                    }
                } catch (NumberFormatException e ) {
                    throw new ValidationException(RC.INPUT_NOT_VALID, RC.INPUT_NOT_VALID_MSG, rc);
                }

                //get Carriers based on carrierId
                Carriers carrier = carriersFacade.find(carrierId);
                if(carrier == null) {
                    throw new ValidationException(RC.CARRIER_NOT_FOUND, RC.CARRIER_NOT_FOUND_MESSAGE, rc);
                }
                
                VehicleCategories vehicleCategory = vehicleCategoriesFacade.find(vehicleCategoryId);
                if(vehicleCategory == null) {
                    throw new ValidationException(RC.VEHICLE_CATEGORY_NOT_FOUND, RC.VEHICLE_CATEGORY_NOT_FOUND_MESSAGE, rc);                    
                }

                ItemCategories itemCategory = itemCategoriesFacade.find(itemCategoryId);
                if(itemCategory == null) {
                    throw new ValidationException(RC.ITEM_CATEGORY_NOT_FOUND, RC.ITEM_CATEGORY_NOT_FOUND_MESSAGE, rc);                    
                }

                //validate sessionId and carrierId
                Accounts account = accountsFacade.findByLoginCarrierId(carrier, sessionId);
                if(account == null) {
                    throw new ValidationException(RC.CARRIER_NOT_LOGIN, RC.CARRIER_NOT_LOGIN_MESSAGE, rc);
                }
                
                //insert into vehicles
                Vehicles vehicle = new Vehicles();
                vehicle.setCarrierId(carrier);
                vehicle.setItemCategoryId(itemCategory);
                vehicle.setVehicleCategoryId(vehicleCategory);
                vehicle.setLicensePlate(platNumber);
                vehicle.setStatus(Constants.VEHICLE_STATUS_AVAILABLE);
                vehiclesFacade.create(vehicle);

                rc.setSuccess();
                rc.print(out);
                
            } catch (ValidationException ve) {
                ve.getResponse().print(out);
            }
        }        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
