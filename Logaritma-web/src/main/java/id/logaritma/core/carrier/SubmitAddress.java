/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.carrier;

import id.logaritma.core.util.RC;
import id.logaritma.core.util.ResponseClass;
import id.logaritma.core.util.ValidationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Accounts;
import models.Addresses;
import models.Carriers;
import models.Cities;
import models.Shippers;
import org.apache.log4j.Logger;
import sessionbeans.AccountsFacadeLocal;
import sessionbeans.AddressesFacadeLocal;
import sessionbeans.CarriersFacadeLocal;
import sessionbeans.CitiesFacadeLocal;
import sessionbeans.ShippersFacadeLocal;

/**
 *
 * @author rudythong
 */
@WebServlet(name = "SubmitAddress", urlPatterns = {"/carrier/submit_address"})
public class SubmitAddress extends HttpServlet {

    @EJB
    private AddressesFacadeLocal addressesFacade;

    @EJB
    private CitiesFacadeLocal citiesFacade;

    @EJB
    private AccountsFacadeLocal accountsFacade;

    @EJB
    private ShippersFacadeLocal shippersFacade;

    @EJB
    private CarriersFacadeLocal carriersFacade;

    private static final Logger log = Logger.getLogger(SubmitAddress.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ResponseClass rc = new ResponseClass(log, request, response);
        try (PrintWriter out = response.getWriter()) {
            try {                
                String sessionId;
                Long carrierId = 0L;
                Long shipperId = 0L;
                String workPhone;
                String addressStr;
                Long cityId;
                Character type;
                BigDecimal latitude;
                BigDecimal longitude;
                try {
                    sessionId = request.getParameter("session_id");
                    if(!request.getParameter("carrier_id").isEmpty()) carrierId = Long.valueOf(request.getParameter("carrier_id"));
                    if(!request.getParameter("shipper_id").isEmpty()) shipperId = Long.valueOf(request.getParameter("shipper_id"));
                    workPhone = request.getParameter("work_phone");
                    addressStr = request.getParameter("address");
                    cityId = Long.valueOf(request.getParameter("city_id"));
                    type = request.getParameter("type").charAt(0);
                    latitude = BigDecimal.valueOf(Long.valueOf(request.getParameter("latitude")));
                    longitude = BigDecimal.valueOf(Long.valueOf(request.getParameter("longitude")));
                    
                } catch (NumberFormatException e) {
                    throw new ValidationException(RC.INPUT_NOT_VALID, RC.INPUT_NOT_VALID_MSG, rc);
                }
                
                //get Carriers based on carrierId
                Carriers carrier = new Carriers();
                if(carrierId != 0L) {
                    carrier = carriersFacade.find(carrierId);
                    if(carrier == null) {
                        throw new ValidationException(RC.CARRIER_NOT_FOUND, RC.CARRIER_NOT_FOUND_MESSAGE, rc);
                    }                    
                    //validate sessionId and carrierId
                    Accounts account = accountsFacade.findByLoginCarrierId(carrier, sessionId);
                    if(account == null) {
                       throw new ValidationException(RC.CARRIER_NOT_LOGIN, RC.CARRIER_NOT_LOGIN_MESSAGE, rc);
                    }
               }

                //get Shippers based on shipperId
                Shippers shipper = new Shippers();
                if(shipperId != 0L) {
                    shipper = shippersFacade.find(shipperId);
                    if(shipper == null) {
                        throw new ValidationException(RC.SHIPPER_NOT_FOUND, RC.SHIPPER_NOT_FOUND_MESSAGE, rc);
                    }                    
                    //validate sessionId and carrierId
                    Accounts account = accountsFacade.findByLoginShipperId(shipper, sessionId);
                    if(account == null) {
                       throw new ValidationException(RC.SHIPPER_NOT_LOGIN, RC.SHIPPER_NOT_LOGIN_MESSAGE, rc);
                    }
                }

                //find  City
                Cities city = citiesFacade.find(cityId);
                if(city == null) {
                    throw new ValidationException(RC.CITY_NOT_FOUND, RC.CITY_NOT_FOUND_MESSAGE, rc);
                }

                //insert into address
                Addresses address = new Addresses();
                address.setAddress(addressStr);
                address.setCarrierId(carrier);
                address.setShipperId(shipper);
                address.setCityId(city);
                address.setType(type);
                address.setLatitude(latitude);
                address.setLongitude(longitude);
                address.setWorkPhone(workPhone);
                addressesFacade.create(address);

                rc.setSuccess();
                rc.print(out);
                
            } catch (ValidationException ve) {
                ve.getResponse().print(out);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
