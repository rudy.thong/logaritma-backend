/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.carrier;

import id.logaritma.core.util.Constants;
import id.logaritma.core.util.RC;
import id.logaritma.core.util.ResponseClass;
import id.logaritma.core.util.ValidationException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Accounts;
import models.Carriers;
import org.apache.log4j.Logger;
import sessionbeans.AccountsFacadeLocal;
import sessionbeans.CarriersFacadeLocal;

/**
 *
 * @author rudythong
 */
@WebServlet(name = "UploadCarrierDocument", urlPatterns = {"/carrier/upload_carrier_document"})
public class UploadCarrierDocument extends HttpServlet {

    @EJB
    private AccountsFacadeLocal accountsFacade;

    @EJB
    private CarriersFacadeLocal carriersFacade;

    private static final Logger log = Logger.getLogger(UploadCarrierDocument.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ResponseClass rc = new ResponseClass(log, request, response);
        try (PrintWriter out = response.getWriter()) {
            try {                
                String sessionId;
                Long carrierId;
                String documentPath;
                Character documentType;
                try {
                    sessionId = request.getParameter("session_id");
                    carrierId = Long.valueOf(request.getParameter("carrier_id"));
                    documentPath = request.getParameter("document_path");
                    documentType = request.getParameter("document_type").charAt(0);
                } catch (NumberFormatException e ) {
                    throw new ValidationException(RC.INPUT_NOT_VALID, RC.INPUT_NOT_VALID_MSG, rc);
                }
                
                //get Carriers based on carrierId
                Carriers carrier = carriersFacade.find(carrierId);
                if(carrier == null) {
                    throw new ValidationException(RC.CARRIER_NOT_FOUND, RC.CARRIER_NOT_FOUND_MESSAGE, rc);
                }

                //validate sessionId and carrierId
                Accounts account = accountsFacade.findByLoginCarrierId(carrier, sessionId);
                if(account == null) {
                    throw new ValidationException(RC.CARRIER_NOT_LOGIN, RC.CARRIER_NOT_LOGIN_MESSAGE, rc);
                }

                //update carrier
                if(documentType.equals(Constants.CARRIER_DOCUMENT_AKTA)) carrier.setDocAktaPath(documentPath);
                if(documentType.equals(Constants.CARRIER_DOCUMENT_DOMISILI)) carrier.setDocDomisiliPath(documentPath);
                if(documentType.equals(Constants.CARRIER_DOCUMENT_SIUP)) carrier.setDocSiupPath(documentPath);
                if(documentType.equals(Constants.CARRIER_DOCUMENT_HO)) carrier.setDocHoPath(documentPath);
                if(documentType.equals(Constants.CARRIER_DOCUMENT_SIUT)) carrier.setDocSiutPath(documentPath);
                if(documentType.equals(Constants.CARRIER_DOCUMENT_IUJPT)) carrier.setDocIujptPath(documentPath);
                                        
                carriersFacade.edit(carrier);

                rc.setSuccess();
                rc.print(out);
                
            } catch (ValidationException ve) {
                ve.getResponse().print(out);
            }
        }        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
