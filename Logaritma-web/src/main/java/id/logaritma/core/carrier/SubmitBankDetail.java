/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.carrier;

import id.logaritma.core.util.Constants;
import id.logaritma.core.util.RC;
import id.logaritma.core.util.ResponseClass;
import id.logaritma.core.util.ValidationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Accounts;
import models.Banks;
import models.Carriers;
import models.Drivers;
import org.apache.log4j.Logger;
import sessionbeans.AccountsFacadeLocal;
import sessionbeans.BanksFacadeLocal;
import sessionbeans.CarriersFacadeLocal;

/**
 *
 * @author rudythong
 */
@WebServlet(name = "SubmitBankDetail", urlPatterns = {"/carrier/submit_bank_detail"})
public class SubmitBankDetail extends HttpServlet {

    @EJB
    private BanksFacadeLocal banksFacade;

    @EJB
    private AccountsFacadeLocal accountsFacade;

    @EJB
    private CarriersFacadeLocal carriersFacade;

    private static final Logger log = Logger.getLogger(SubmitBankDetail.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ResponseClass rc = new ResponseClass(log, request, response);
        try (PrintWriter out = response.getWriter()) {
            try {                
                String sessionId;
                Long carrierId;
                Long bankId;
                Long bankAccountNumber;
                String bankBranch;
                try {
                    sessionId = request.getParameter("session_id");
                    carrierId = Long.valueOf(request.getParameter("carrier_id"));
                    bankId = Long.valueOf(request.getParameter("bank_id"));
                    bankAccountNumber = Long.valueOf(request.getParameter("bank_account_number"));
                    bankBranch = request.getParameter("bank_branch");
                } catch (NumberFormatException e) {
                    throw new ValidationException(RC.INPUT_NOT_VALID, RC.INPUT_NOT_VALID_MSG, rc);
                }
                
                //get Carriers based on carrierId
                Carriers carrier = carriersFacade.find(carrierId);
                if(carrier == null) {
                    throw new ValidationException(RC.CARRIER_NOT_FOUND, RC.CARRIER_NOT_FOUND_MESSAGE, rc);
                }

                //validate sessionId and carrierId
                Accounts account = accountsFacade.findByLoginCarrierId(carrier, sessionId);
                if(account == null) {
                    throw new ValidationException(RC.CARRIER_NOT_LOGIN, RC.CARRIER_NOT_LOGIN_MESSAGE, rc);
                }
                
                Banks bank = banksFacade.find(bankId);
                if(bank == null) {
                    throw new ValidationException(RC.BANK_NOT_FOUND, RC.BANK_NOT_FOUND_MESSAGE);
                }

                //update carrier
                carrier.setBankId(bank);
                carrier.setBankAccountNumber(bankAccountNumber);
                carrier.setBankBranch(bankBranch);
                carriersFacade.edit(carrier);
                
                rc.setSuccess();
                rc.print(out);
                
            } catch (ValidationException ve) {
                ve.getResponse().print(out);
            }
        }            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
