/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.carrier;

import id.logaritma.core.util.Constants;
import id.logaritma.core.util.RC;
import id.logaritma.core.util.ResponseClass;
import id.logaritma.core.util.ValidationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Accounts;
import models.Carriers;
import models.Cities;
import models.Drivers;
import models.Jobs;
import models.Vehicles;
import org.apache.log4j.Logger;
import sessionbeans.AccountsFacadeLocal;
import sessionbeans.CarriersFacadeLocal;
import sessionbeans.CitiesFacadeLocal;
import sessionbeans.DriversFacadeLocal;
import sessionbeans.JobsFacadeLocal;
import sessionbeans.VehiclesFacadeLocal;

/**
 *
 * @author rudythong
 */
@WebServlet(name = "InsertBackload", urlPatterns = {"/carrier/insert_backload"})
public class InsertBackload extends HttpServlet {

    @EJB
    private VehiclesFacadeLocal vehiclesFacade;

    @EJB
    private CitiesFacadeLocal citiesFacade;

    @EJB
    private CarriersFacadeLocal carriersFacade;

    @EJB
    private AccountsFacadeLocal accountsFacade;

    @EJB
    private DriversFacadeLocal driversFacade;

    @EJB
    private JobsFacadeLocal jobsFacade;

    private static final Logger log = Logger.getLogger(InsertBackload.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ResponseClass rc = new ResponseClass(log, request, response);
        try (PrintWriter out = response.getWriter()) {
            try {                
                String sessionId;
                Long carrierId;
                Long vehicleId;
                Long driverId;
                Long originCityId;
                Long destinationCityId;
                Date startDate;
                Date endDate;
                try {
                    sessionId = request.getParameter("session_id");
                    carrierId = Long.valueOf(request.getParameter("carrier_id"));
                    vehicleId = Long.valueOf(request.getParameter("vehicle_id"));
                    driverId = Long.valueOf(request.getParameter("driver_id"));
                    originCityId = Long.valueOf(request.getParameter("origin_city_id"));    
                    destinationCityId = Long.valueOf(request.getParameter("destination_city_id"));
                    startDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse(request.getParameter("start_date"));
                    endDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse(request.getParameter("end_date"));
                } catch (NumberFormatException | ParseException e ) {
                    throw new ValidationException(RC.INPUT_NOT_VALID, RC.INPUT_NOT_VALID_MSG, rc);
                }
                
                //get Drivers based on driverId
                Drivers driver = driversFacade.find(driverId);
                if(driver == null) {
                    throw new ValidationException(RC.DRIVER_NOT_FOUND, RC.DRIVER_NOT_FOUND_MESSAGE, rc);
                }

                //get Vehicles based on vehicleId
                Vehicles vehicle = vehiclesFacade.find(vehicleId);
                if(vehicle == null) {
                    throw new ValidationException(RC.VEHICLE_NOT_FOUND, RC.VEHICLE_NOT_FOUND_MESSAGE, rc);
                }

                //get Carriers based on carrierId
                Carriers carrier = carriersFacade.find(carrierId);
                if(carrier == null) {
                    throw new ValidationException(RC.CARRIER_NOT_FOUND, RC.CARRIER_NOT_FOUND_MESSAGE, rc);
                }

                //validate Driver owned by Carrier
                if(driver.getCarrierId() != carrier) {
                    throw new ValidationException(RC.DRIVER_CARRIER_NOT_MATCH, RC.DRIVER_CARRIER_NOT_MATCH_MESSAGE, rc);
                }

                //validate Vehicle owner by Carrier
                if(vehicle.getCarrierId() != carrier) {
                    throw new ValidationException(RC.VEHICLE_CARRIER_NOT_MATCH, RC.VEHICLE_CARRIER_NOT_MATCH_MESSAGE, rc);
                }

                //validate sessionId and carrierId
                Accounts account = accountsFacade.findByLoginCarrierId(carrier, sessionId);
                if(account == null) {
                    throw new ValidationException(RC.CARRIER_NOT_LOGIN, RC.CARRIER_NOT_LOGIN_MESSAGE, rc);
                }

                //find Origin City
                Cities originCity = citiesFacade.find(originCityId);
                if(originCity == null) {
                    throw new ValidationException(RC.CITY_NOT_FOUND, RC.CITY_NOT_FOUND_MESSAGE, rc);
                }
                
                Cities destinationCity = citiesFacade.find(destinationCityId);
                if(destinationCity == null) {
                    throw new ValidationException(RC.CITY_NOT_FOUND, RC.CITY_NOT_FOUND_MESSAGE, rc);
                }

                //insert into job
                Jobs job = new Jobs();
                job.setOriginCityId(originCity);
                job.setDestinationCityId(destinationCity);
                job.setStartTimestamp(startDate);
                job.setEndTimestamp(endDate);
                job.setDriverId(driver);
                job.setVehicleId(vehicle);
                job.setStatus(Constants.JOB_STATUS_ACTIVE);
                jobsFacade.create(job);

                rc.setSuccess();
                rc.print(out);
                
            } catch (ValidationException ve) {
                ve.getResponse().print(out);
            }
        }    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
