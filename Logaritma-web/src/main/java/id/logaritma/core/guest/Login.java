/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.guest;

import id.logaritma.core.util.Constants;
import id.logaritma.core.util.RC;
import id.logaritma.core.util.ValidationException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Accounts;
import models.Carriers;
import models.Shippers;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;
import sessionbeans.AccountsFacadeLocal;
import sessionbeans.CarriersFacadeLocal;
import sessionbeans.OrdersFacadeLocal;
import sessionbeans.ShippersFacadeLocal;

/**
 *
 * @author Herianto
 */
@WebServlet(name = "Login", urlPatterns = {"/guest/login"})
public class Login extends HttpServlet {

    @EJB
    private CarriersFacadeLocal carriersFacade;

    @EJB
    private ShippersFacadeLocal shippersFacade;

    @EJB
    private OrdersFacadeLocal ordersFacade;

    @EJB
    private AccountsFacadeLocal accountsFacade;
     
    private static final Logger log = Logger.getLogger(Login.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String sessionId = request.getSession().getId();
        MDC.put("sessionId", sessionId);
        log.info("LOGIN START");
        String responseCode = RC.ERROR;
        String responseMessage = RC.ERROR_MESSAGE;
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            try{
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                int type = 0;
                long id = 0;
                int total_onjobs = 0;
                int total_pendingjobs = 0;
                Shippers shipper;
                Carriers carrier;
                String companyName;
                log.info("Username = " + username);
                log.info("Password = " + password);
                Accounts account = accountsFacade.findByUsername(username);
                if(account == null) throw new ValidationException(RC.LOGIN_FAILED, RC.LOGIN_FAILED_MESSAGE);
                else{
                    if(!DigestUtils.sha1Hex(password).equals(account.getPassword())) throw new ValidationException(RC.LOGIN_FAILED, RC.LOGIN_FAILED_MESSAGE);
                    account.setCurrentSession(sessionId);
                    accountsFacade.edit(account);
                    if(account.getCarrierId() == null){
                        type = 1;
                        id = account.getShipperId().getId();
                        total_onjobs = ordersFacade.getTotalJobsByShipperIdandStatus(account.getShipperId(), Constants.JOB_STATUS_ACTIVE);
                        total_pendingjobs = ordersFacade.getTotalJobsByShipperIdandStatus(account.getShipperId(), Constants.JOB_STATUS_NOT_MATCHED);
                        shipper = shippersFacade.find(account.getShipperId());
                        companyName = shipper.getCompanyName();
                    }
                    else {
                        id = account.getCarrierId().getId();
                        carrier = carriersFacade.find(account.getCarrierId());
                        companyName = carrier.getCompanyName();
                    }
                    responseCode = RC.SUCCESS;
                    responseMessage = RC.SUCCESS_MSG;
                }
                JSONObject json = new JSONObject();
                json.put("status", responseCode);
                json.put("message", responseMessage);
                JSONObject data = new JSONObject();
                data.put("type", type);
                data.put("id", id);
                data.put("sessionId", sessionId);
                data.put("total_on_jobs", total_onjobs);
                data.put("total_pending_jobs", total_pendingjobs);
                data.put("account_name", account.getName());
                data.put("image_name", account.getImagePath());
                data.put("company_name", companyName);
                json.put("data", data);
                out.print(json.toString(Constants.JSON_INDENT_FACTOR));
            }
            catch (ValidationException ve) {
                responseCode = ve.getCode();
                responseMessage = ve.getMessage();
                log.error("VALIDATION EXCEPTION = " + ve.getMessage());
            }
            catch (NumberFormatException | JSONException e) {
                log.error("UNEXPECTED EXCEPTION = " + e.getMessage());
                JSONObject json = new JSONObject();
                json.put("status", responseCode);
                json.put("message", responseMessage);
                out.print(json.toString(Constants.JSON_INDENT_FACTOR));
            } finally {
                out.close();                
            }
        }
        log.info("LOGIN END");        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
