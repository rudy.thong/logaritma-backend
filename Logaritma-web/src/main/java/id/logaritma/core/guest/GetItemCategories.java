/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.guest;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.ItemCategories;
import org.json.JSONArray;
import org.json.JSONObject;
import sessionbeans.ItemCategoriesFacadeLocal;

/**
 *
 * @author rudythong
 */
@WebServlet(name = "GetItemCategories", urlPatterns = {"/guest/get_item_categories"})
public class GetItemCategories extends HttpServlet {

    @EJB
    private ItemCategoriesFacadeLocal itemCategoriesFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            List<ItemCategories> itemCategories = itemCategoriesFacade.findAll();
            JSONArray arrItemCategories = new JSONArray();
            for(ItemCategories itemCategory : itemCategories) {
                JSONObject jsonItemCategory = new JSONObject();
                jsonItemCategory.put("id", itemCategory.getId());
                jsonItemCategory.put("name", itemCategory.getName());
                jsonItemCategory.put("examples", itemCategory.getExamples());
                jsonItemCategory.put("additional_price_multiply", itemCategory.getAdditionalPriceMultiply());
                arrItemCategories.put(jsonItemCategory);
            }
            out.println(arrItemCategories.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
