/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.guest;

import id.logaritma.core.util.Constants;
import id.logaritma.core.util.RC;
import id.logaritma.core.util.Util;
import id.logaritma.core.util.ValidationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Accounts;
import models.EmailNotifications;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.json.JSONException;
import org.json.JSONObject;
import sessionbeans.AccountsFacadeLocal;
import sessionbeans.ConfigFacadeLocal;
import sessionbeans.EmailNotificationsFacadeLocal;

/**
 *
 * @author Herianto
 */
@WebServlet(name = "ForgotPassword", urlPatterns = {"/guest/forgot_password"})
public class ForgotPassword extends HttpServlet {

    @EJB
    private ConfigFacadeLocal configFacade;

    @EJB
    private EmailNotificationsFacadeLocal emailNotificationsFacade;
    
    @EJB
    private AccountsFacadeLocal accountsFacade;
     
    private static final Logger log = Logger.getLogger(ForgotPassword.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String sessionId = request.getSession().getId();
        MDC.put("sessionId", sessionId);
        log.info("FORGOT PASSWORD START");
        String responseCode = RC.ERROR;
        String responseMessage = RC.ERROR_MESSAGE;
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            try{
                String email = request.getParameter("email");
                log.info("Email = " + email);
                Accounts account = accountsFacade.findByEmail(email);
                if(account == null) throw new ValidationException(RC.LOGIN_FAILED, RC.LOGIN_FAILED_MESSAGE);
                else{
                    String password = Util.getRandomPass(8);
                    account.setPassword(DigestUtils.sha1Hex(password));
                    accountsFacade.edit(account);
                    // insert into table email_notification
                    EmailNotifications notif = new EmailNotifications();
                    notif.setSubject(configFacade.find("forgotpass.subject").getValues());
                    notif.setEmailTo(email);
                    notif.setContentPath(configFacade.find("forgotpass.content.path").getValues());
                    notif.setStatus(Constants.EMAIL_SCHEDULER_PENDING);
                    notif.setLastUpdated(new Date());
                    emailNotificationsFacade.create(notif);
                    responseCode = RC.SUCCESS;
                    responseMessage = RC.SUCCESS_MSG;
                }
                JSONObject json = new JSONObject();
                json.put("status", responseCode);
                json.put("message", responseMessage);
                JSONObject data = new JSONObject();
                data.put("email", email);
                json.put("data", data);
                out.print(json.toString(Constants.JSON_INDENT_FACTOR));
            }
            catch (ValidationException ve) {
                responseCode = ve.getCode();
                responseMessage = ve.getMessage();
                log.error("VALIDATION EXCEPTION = " + ve.getMessage());
            }
            catch (NumberFormatException | JSONException e) {
                log.error("UNEXPECTED EXCEPTION = " + e.getMessage());
                JSONObject json = new JSONObject();
                json.put("status", responseCode);
                json.put("message", responseMessage);
                out.print(json.toString(Constants.JSON_INDENT_FACTOR));
            } finally {
                out.close();                
            }
        }
        log.info("FORGOT PASSWORD END");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
