/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.guest;

import id.logaritma.core.util.Constants;
import id.logaritma.core.util.Gmaps;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import id.logaritma.core.util.RC;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.ejb.EJB;
import models.VehicleCategories;

/**
 *
 * @author Herianto
 */
@WebServlet(name = "CalculatePricePublic", urlPatterns = {"/guest/calculate_price_public"})
public class CalculatePricePublic extends HttpServlet {

    @EJB
    private sessionbeans.VehicleCategoriesFacadeLocal vehicleCategoriesFacade;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            VehicleCategories vehicleCategories = vehicleCategoriesFacade.find(1);
            BigDecimal pricePerKm = vehicleCategories.getPricePerKm();
            BigDecimal finalPrice = new BigDecimal(Gmaps.computeDistance(0, 0, 0, 0, 0, 0)).multiply(pricePerKm);
            JSONObject json = new JSONObject();
            json.put("status", RC.SUCCESS);
            json.put("message", RC.SUCCESS_MSG);
            JSONObject data = new JSONObject();
            data.put("PRICE", finalPrice);
            out.print(json.toString(Constants.JSON_INDENT_FACTOR));
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
