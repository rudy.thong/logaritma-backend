/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.logaritma.core.guest;

import id.logaritma.core.util.Constants;
import id.logaritma.core.util.RC;
import id.logaritma.core.util.Util;
import id.logaritma.core.util.ValidationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Accounts;
import models.Addresses;
import models.Carriers;
import models.Cities;
import models.Shippers;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.json.JSONException;
import org.json.JSONObject;
import sessionbeans.AccountsFacadeLocal;
import sessionbeans.AddressesFacadeLocal;
import sessionbeans.CarriersFacadeLocal;
import sessionbeans.CitiesFacadeLocal;
import sessionbeans.ShippersFacadeLocal;

/**
 *
 * @author Herianto
 */
@WebServlet(name = "Register", urlPatterns = {"/guest/register"})
public class Register extends HttpServlet {

    @EJB
    private CitiesFacadeLocal citiesFacade;

    @EJB
    private AddressesFacadeLocal addressesFacade;

    @EJB
    private ShippersFacadeLocal shippersFacade;

    @EJB
    private CarriersFacadeLocal carriersFacade;
    
    @EJB
    private AccountsFacadeLocal accountsFacade;
     
    private static final Logger log = Logger.getLogger(Register.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("REGISTER START");
        String responseCode = RC.SUCCESS;
        String responseMessage = RC.SUCCESS_MSG;
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            try{
                int type = Integer.parseInt(request.getParameter("type"));
                String email = request.getParameter("email");
                String accountName = request.getParameter("account_name");
                String companyName = request.getParameter("company_name");
                String phone = request.getParameter("phone");
                String registered_address = request.getParameter("address");
                BigInteger province = new BigInteger(request.getParameter("province"));
                BigInteger cityId = new BigInteger(request.getParameter("city"));
                long id = 0;
                log.info("Type = " + type);
                log.info("Email = " + email);
                log.info("Account Name = " + accountName);
                log.info("Company Name = " + companyName);
                log.info("Phone = " + phone);
                log.info("Address = " + registered_address);
                log.info("Province = " + province);
                log.info("City = " + cityId);
                Accounts account = accountsFacade.findByEmail(email);
                Cities city = citiesFacade.find(cityId);
                // check if email existed
                if(account != null) throw new ValidationException(RC.REGISTRATION_EMAIL_EXISTED, RC.REGISTRATION_EMAIL_EXISTED_MESSAGE);
                else{
                    //check type shipper or carrier
                    account = new Accounts();
                    Addresses address = new Addresses();
                    switch(type){
                        case 1 :
                            Shippers shippers = new Shippers();
                            shippers.setCompanyName(companyName);
                            shippersFacade.create(shippers);
                            account.setShipperId(shippers);
                            id = shippers.getId();
                            address.setShipperId(shippers);
                        default :
                            Carriers carriers = new Carriers();
                            carriers.setCompanyName(companyName);
                            carriersFacade.create(carriers);
                            account.setCarrierId(carriers);
                            id = carriers.getId();
                            address.setCarrierId(carriers);
                    }
                    account.setUsername(email);
                    account.setEmail(email);
                    account.setName(accountName);
                    account.setPhone(phone);
                    String password = Util.getRandomPass(8);
                    account.setPassword(DigestUtils.sha1Hex(password));
                    accountsFacade.create(account);
                    address.setAddress(registered_address);
                    address.setCityId(city);
                    addressesFacade.create(address);
                }
                JSONObject json = new JSONObject();
                json.put("status", responseCode);
                json.put("message", responseMessage);
                JSONObject data = new JSONObject();
                data.put("type", type);
                data.put("id", id);
                json.put("data", data);
                out.print(json.toString(Constants.JSON_INDENT_FACTOR));
            }
            catch (ValidationException ve) {
                responseCode = ve.getCode();
                responseMessage = ve.getMessage();
                log.error("VALIDATION EXCEPTION = " + ve.getMessage());
            }
            catch (NumberFormatException | JSONException e) {
                log.error("UNEXPECTED EXCEPTION = " + e.getMessage());
                JSONObject json = new JSONObject();
                json.put("status", responseCode);
                json.put("message", responseMessage);
                out.print(json.toString(Constants.JSON_INDENT_FACTOR));
            } finally {
                out.close();                
            }
        }
        log.info("REGISTER END"); 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
