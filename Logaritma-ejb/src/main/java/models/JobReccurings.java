/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "job_reccurings")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JobReccurings.findAll", query = "SELECT j FROM JobReccurings j"),
    @NamedQuery(name = "JobReccurings.findById", query = "SELECT j FROM JobReccurings j WHERE j.id = :id"),
    @NamedQuery(name = "JobReccurings.findByRecurType", query = "SELECT j FROM JobReccurings j WHERE j.recurType = :recurType"),
    @NamedQuery(name = "JobReccurings.findByMaxRecur", query = "SELECT j FROM JobReccurings j WHERE j.maxRecur = :maxRecur")})
public class JobReccurings implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "recur_type")
    private Character recurType;
    @Column(name = "max_recur")
    private Integer maxRecur;
    @JoinColumn(name = "job_id", referencedColumnName = "id")
    @ManyToOne
    private Jobs jobId;

    public JobReccurings() {
    }

    public JobReccurings(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getRecurType() {
        return recurType;
    }

    public void setRecurType(Character recurType) {
        this.recurType = recurType;
    }

    public Integer getMaxRecur() {
        return maxRecur;
    }

    public void setMaxRecur(Integer maxRecur) {
        this.maxRecur = maxRecur;
    }

    public Jobs getJobId() {
        return jobId;
    }

    public void setJobId(Jobs jobId) {
        this.jobId = jobId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JobReccurings)) {
            return false;
        }
        JobReccurings other = (JobReccurings) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.JobReccurings[ id=" + id + " ]";
    }
    
}
