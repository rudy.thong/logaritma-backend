/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "jobs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jobs.findAll", query = "SELECT j FROM Jobs j"),
    @NamedQuery(name = "Jobs.findById", query = "SELECT j FROM Jobs j WHERE j.id = :id"),
    @NamedQuery(name = "Jobs.findByStartTimestamp", query = "SELECT j FROM Jobs j WHERE j.startTimestamp = :startTimestamp"),
    @NamedQuery(name = "Jobs.findByEndTimestamp", query = "SELECT j FROM Jobs j WHERE j.endTimestamp = :endTimestamp"),
    @NamedQuery(name = "Jobs.findByStatus", query = "SELECT j FROM Jobs j WHERE j.status = :status")})
public class Jobs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "start_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTimestamp;
    @Column(name = "end_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTimestamp;
    @Column(name = "status")
    private Character status;
    @JoinColumn(name = "origin_city_id", referencedColumnName = "id")
    @ManyToOne
    private Cities originCityId;
    @JoinColumn(name = "destination_city_id", referencedColumnName = "id")
    @ManyToOne
    private Cities destinationCityId;
    @JoinColumn(name = "driver_id", referencedColumnName = "id")
    @ManyToOne
    private Drivers driverId;
    @JoinColumn(name = "vehicle_id", referencedColumnName = "id")
    @ManyToOne
    private Vehicles vehicleId;
    @OneToMany(mappedBy = "jobId")
    private Collection<JobReccurings> jobReccuringsCollection;
    @OneToMany(mappedBy = "jobId")
    private Collection<Orders> ordersCollection;

    public Jobs() {
    }

    public Jobs(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Date startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public Date getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(Date endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Cities getOriginCityId() {
        return originCityId;
    }

    public void setOriginCityId(Cities originCityId) {
        this.originCityId = originCityId;
    }

    public Cities getDestinationCityId() {
        return destinationCityId;
    }

    public void setDestinationCityId(Cities destinationCityId) {
        this.destinationCityId = destinationCityId;
    }

    public Drivers getDriverId() {
        return driverId;
    }

    public void setDriverId(Drivers driverId) {
        this.driverId = driverId;
    }

    public Vehicles getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Vehicles vehicleId) {
        this.vehicleId = vehicleId;
    }

    @XmlTransient
    public Collection<JobReccurings> getJobReccuringsCollection() {
        return jobReccuringsCollection;
    }

    public void setJobReccuringsCollection(Collection<JobReccurings> jobReccuringsCollection) {
        this.jobReccuringsCollection = jobReccuringsCollection;
    }

    @XmlTransient
    public Collection<Orders> getOrdersCollection() {
        return ordersCollection;
    }

    public void setOrdersCollection(Collection<Orders> ordersCollection) {
        this.ordersCollection = ordersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jobs)) {
            return false;
        }
        Jobs other = (Jobs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Jobs[ id=" + id + " ]";
    }
    
}
