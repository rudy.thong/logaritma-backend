/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "shippers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Shippers.findAll", query = "SELECT s FROM Shippers s"),
    @NamedQuery(name = "Shippers.findById", query = "SELECT s FROM Shippers s WHERE s.id = :id"),
    @NamedQuery(name = "Shippers.findByCompanyName", query = "SELECT s FROM Shippers s WHERE s.companyName = :companyName"),
    @NamedQuery(name = "Shippers.findByDocAktaPath", query = "SELECT s FROM Shippers s WHERE s.docAktaPath = :docAktaPath"),
    @NamedQuery(name = "Shippers.findByDocNpwpPath", query = "SELECT s FROM Shippers s WHERE s.docNpwpPath = :docNpwpPath"),
    @NamedQuery(name = "Shippers.findByDocDomisiliPath", query = "SELECT s FROM Shippers s WHERE s.docDomisiliPath = :docDomisiliPath"),
    @NamedQuery(name = "Shippers.findByDocSiupPath", query = "SELECT s FROM Shippers s WHERE s.docSiupPath = :docSiupPath"),
    @NamedQuery(name = "Shippers.findByP2pLenderAccount", query = "SELECT s FROM Shippers s WHERE s.p2pLenderAccount = :p2pLenderAccount"),
    @NamedQuery(name = "Shippers.findByAverageRating", query = "SELECT s FROM Shippers s WHERE s.averageRating = :averageRating")})
public class Shippers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 100)
    @Column(name = "company_name")
    private String companyName;
    @Size(max = 255)
    @Column(name = "doc_akta_path")
    private String docAktaPath;
    @Size(max = 255)
    @Column(name = "doc_npwp_path")
    private String docNpwpPath;
    @Size(max = 255)
    @Column(name = "doc_domisili_path")
    private String docDomisiliPath;
    @Size(max = 255)
    @Column(name = "doc_siup_path")
    private String docSiupPath;
    @Size(max = 50)
    @Column(name = "p2p_lender_account")
    private String p2pLenderAccount;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "average_rating")
    private BigDecimal averageRating;
    @OneToMany(mappedBy = "shipperId")
    private Collection<Addresses> addressesCollection;
    @JoinColumn(name = "p2p_lender_id", referencedColumnName = "id")
    @ManyToOne
    private P2pLenders p2pLenderId;
    @OneToMany(mappedBy = "shipperId")
    private Collection<ShipperContacts> shipperContactsCollection;
    @OneToMany(mappedBy = "shipperId")
    private Collection<HistoryShipperP2pLenders> historyShipperP2pLendersCollection;
    @OneToMany(mappedBy = "shipperId")
    private Collection<Orders> ordersCollection;
    @OneToMany(mappedBy = "shipperId")
    private Collection<Accounts> accountsCollection;

    public Shippers() {
    }

    public Shippers(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDocAktaPath() {
        return docAktaPath;
    }

    public void setDocAktaPath(String docAktaPath) {
        this.docAktaPath = docAktaPath;
    }

    public String getDocNpwpPath() {
        return docNpwpPath;
    }

    public void setDocNpwpPath(String docNpwpPath) {
        this.docNpwpPath = docNpwpPath;
    }

    public String getDocDomisiliPath() {
        return docDomisiliPath;
    }

    public void setDocDomisiliPath(String docDomisiliPath) {
        this.docDomisiliPath = docDomisiliPath;
    }

    public String getDocSiupPath() {
        return docSiupPath;
    }

    public void setDocSiupPath(String docSiupPath) {
        this.docSiupPath = docSiupPath;
    }

    public String getP2pLenderAccount() {
        return p2pLenderAccount;
    }

    public void setP2pLenderAccount(String p2pLenderAccount) {
        this.p2pLenderAccount = p2pLenderAccount;
    }

    public BigDecimal getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(BigDecimal averageRating) {
        this.averageRating = averageRating;
    }

    @XmlTransient
    public Collection<Addresses> getAddressesCollection() {
        return addressesCollection;
    }

    public void setAddressesCollection(Collection<Addresses> addressesCollection) {
        this.addressesCollection = addressesCollection;
    }

    public P2pLenders getP2pLenderId() {
        return p2pLenderId;
    }

    public void setP2pLenderId(P2pLenders p2pLenderId) {
        this.p2pLenderId = p2pLenderId;
    }

    @XmlTransient
    public Collection<ShipperContacts> getShipperContactsCollection() {
        return shipperContactsCollection;
    }

    public void setShipperContactsCollection(Collection<ShipperContacts> shipperContactsCollection) {
        this.shipperContactsCollection = shipperContactsCollection;
    }

    @XmlTransient
    public Collection<HistoryShipperP2pLenders> getHistoryShipperP2pLendersCollection() {
        return historyShipperP2pLendersCollection;
    }

    public void setHistoryShipperP2pLendersCollection(Collection<HistoryShipperP2pLenders> historyShipperP2pLendersCollection) {
        this.historyShipperP2pLendersCollection = historyShipperP2pLendersCollection;
    }

    @XmlTransient
    public Collection<Orders> getOrdersCollection() {
        return ordersCollection;
    }

    public void setOrdersCollection(Collection<Orders> ordersCollection) {
        this.ordersCollection = ordersCollection;
    }

    @XmlTransient
    public Collection<Accounts> getAccountsCollection() {
        return accountsCollection;
    }

    public void setAccountsCollection(Collection<Accounts> accountsCollection) {
        this.accountsCollection = accountsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Shippers)) {
            return false;
        }
        Shippers other = (Shippers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Shippers[ id=" + id + " ]";
    }
    
}
