/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "vehicle_categories")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VehicleCategories.findAll", query = "SELECT v FROM VehicleCategories v"),
    @NamedQuery(name = "VehicleCategories.findById", query = "SELECT v FROM VehicleCategories v WHERE v.id = :id"),
    @NamedQuery(name = "VehicleCategories.findByName", query = "SELECT v FROM VehicleCategories v WHERE v.name = :name"),
    @NamedQuery(name = "VehicleCategories.findByPricePerKm", query = "SELECT v FROM VehicleCategories v WHERE v.pricePerKm = :pricePerKm")})
public class VehicleCategories implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price_per_km")
    private BigDecimal pricePerKm;

    public VehicleCategories() {
    }

    public VehicleCategories(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPricePerKm() {
        return pricePerKm;
    }

    public void setPricePerKm(BigDecimal pricePerKm) {
        this.pricePerKm = pricePerKm;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleCategories)) {
            return false;
        }
        VehicleCategories other = (VehicleCategories) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.VehicleCategories[ id=" + id + " ]";
    }
    
}
