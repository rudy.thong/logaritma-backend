/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "carriers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Carriers.findAll", query = "SELECT c FROM Carriers c"),
    @NamedQuery(name = "Carriers.findById", query = "SELECT c FROM Carriers c WHERE c.id = :id"),
    @NamedQuery(name = "Carriers.findByCompanyName", query = "SELECT c FROM Carriers c WHERE c.companyName = :companyName"),
    @NamedQuery(name = "Carriers.findByDotNumber", query = "SELECT c FROM Carriers c WHERE c.dotNumber = :dotNumber"),
    @NamedQuery(name = "Carriers.findByDocAktaPath", query = "SELECT c FROM Carriers c WHERE c.docAktaPath = :docAktaPath"),
    @NamedQuery(name = "Carriers.findByDocDomisiliPath", query = "SELECT c FROM Carriers c WHERE c.docDomisiliPath = :docDomisiliPath"),
    @NamedQuery(name = "Carriers.findByDocSiupPath", query = "SELECT c FROM Carriers c WHERE c.docSiupPath = :docSiupPath"),
    @NamedQuery(name = "Carriers.findByDocHoPath", query = "SELECT c FROM Carriers c WHERE c.docHoPath = :docHoPath"),
    @NamedQuery(name = "Carriers.findByDocSiutPath", query = "SELECT c FROM Carriers c WHERE c.docSiutPath = :docSiutPath"),
    @NamedQuery(name = "Carriers.findByDocIujptPath", query = "SELECT c FROM Carriers c WHERE c.docIujptPath = :docIujptPath"),
    @NamedQuery(name = "Carriers.findByBankAccountNumber", query = "SELECT c FROM Carriers c WHERE c.bankAccountNumber = :bankAccountNumber"),
    @NamedQuery(name = "Carriers.findByBankBranch", query = "SELECT c FROM Carriers c WHERE c.bankBranch = :bankBranch"),
    @NamedQuery(name = "Carriers.findByStatus", query = "SELECT c FROM Carriers c WHERE c.status = :status"),
    @NamedQuery(name = "Carriers.findByDocNpwpPath", query = "SELECT c FROM Carriers c WHERE c.docNpwpPath = :docNpwpPath"),
    @NamedQuery(name = "Carriers.findByDocCargoInsurance", query = "SELECT c FROM Carriers c WHERE c.docCargoInsurance = :docCargoInsurance"),
    @NamedQuery(name = "Carriers.findByDocVehicleInsurance", query = "SELECT c FROM Carriers c WHERE c.docVehicleInsurance = :docVehicleInsurance"),
    @NamedQuery(name = "Carriers.findByAverageRating", query = "SELECT c FROM Carriers c WHERE c.averageRating = :averageRating")})
public class Carriers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 100)
    @Column(name = "company_name")
    private String companyName;
    @Size(max = 100)
    @Column(name = "dot_number")
    private String dotNumber;
    @Size(max = 255)
    @Column(name = "doc_akta_path")
    private String docAktaPath;
    @Size(max = 255)
    @Column(name = "doc_domisili_path")
    private String docDomisiliPath;
    @Size(max = 255)
    @Column(name = "doc_siup_path")
    private String docSiupPath;
    @Size(max = 255)
    @Column(name = "doc_ho_path")
    private String docHoPath;
    @Size(max = 255)
    @Column(name = "doc_siut_path")
    private String docSiutPath;
    @Size(max = 255)
    @Column(name = "doc_iujpt_path")
    private String docIujptPath;
    @Column(name = "bank_account_number")
    private Long bankAccountNumber;
    @Size(max = 255)
    @Column(name = "bank_branch")
    private String bankBranch;
    @Column(name = "status")
    private Character status;
    @Size(max = 255)
    @Column(name = "doc_npwp_path")
    private String docNpwpPath;
    @Size(max = 255)
    @Column(name = "doc_cargo_insurance")
    private String docCargoInsurance;
    @Size(max = 255)
    @Column(name = "doc_vehicle_insurance")
    private String docVehicleInsurance;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "average_rating")
    private BigDecimal averageRating;
    @OneToMany(mappedBy = "carrierId")
    private Collection<Addresses> addressesCollection;
    @OneToMany(mappedBy = "carrierId")
    private Collection<Vehicles> vehiclesCollection;
    @OneToMany(mappedBy = "carrierId")
    private Collection<CarrierCoverages> carrierCoveragesCollection;
    @JoinColumn(name = "bank_id", referencedColumnName = "id")
    @ManyToOne
    private Banks bankId;
    @OneToMany(mappedBy = "carrierId")
    private Collection<Accounts> accountsCollection;
    @OneToMany(mappedBy = "carrierId")
    private Collection<Drivers> driversCollection;

    public Carriers() {
    }

    public Carriers(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDotNumber() {
        return dotNumber;
    }

    public void setDotNumber(String dotNumber) {
        this.dotNumber = dotNumber;
    }

    public String getDocAktaPath() {
        return docAktaPath;
    }

    public void setDocAktaPath(String docAktaPath) {
        this.docAktaPath = docAktaPath;
    }

    public String getDocDomisiliPath() {
        return docDomisiliPath;
    }

    public void setDocDomisiliPath(String docDomisiliPath) {
        this.docDomisiliPath = docDomisiliPath;
    }

    public String getDocSiupPath() {
        return docSiupPath;
    }

    public void setDocSiupPath(String docSiupPath) {
        this.docSiupPath = docSiupPath;
    }

    public String getDocHoPath() {
        return docHoPath;
    }

    public void setDocHoPath(String docHoPath) {
        this.docHoPath = docHoPath;
    }

    public String getDocSiutPath() {
        return docSiutPath;
    }

    public void setDocSiutPath(String docSiutPath) {
        this.docSiutPath = docSiutPath;
    }

    public String getDocIujptPath() {
        return docIujptPath;
    }

    public void setDocIujptPath(String docIujptPath) {
        this.docIujptPath = docIujptPath;
    }

    public Long getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(Long bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getDocNpwpPath() {
        return docNpwpPath;
    }

    public void setDocNpwpPath(String docNpwpPath) {
        this.docNpwpPath = docNpwpPath;
    }

    public String getDocCargoInsurance() {
        return docCargoInsurance;
    }

    public void setDocCargoInsurance(String docCargoInsurance) {
        this.docCargoInsurance = docCargoInsurance;
    }

    public String getDocVehicleInsurance() {
        return docVehicleInsurance;
    }

    public void setDocVehicleInsurance(String docVehicleInsurance) {
        this.docVehicleInsurance = docVehicleInsurance;
    }

    public BigDecimal getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(BigDecimal averageRating) {
        this.averageRating = averageRating;
    }

    @XmlTransient
    public Collection<Addresses> getAddressesCollection() {
        return addressesCollection;
    }

    public void setAddressesCollection(Collection<Addresses> addressesCollection) {
        this.addressesCollection = addressesCollection;
    }

    @XmlTransient
    public Collection<Vehicles> getVehiclesCollection() {
        return vehiclesCollection;
    }

    public void setVehiclesCollection(Collection<Vehicles> vehiclesCollection) {
        this.vehiclesCollection = vehiclesCollection;
    }

    @XmlTransient
    public Collection<CarrierCoverages> getCarrierCoveragesCollection() {
        return carrierCoveragesCollection;
    }

    public void setCarrierCoveragesCollection(Collection<CarrierCoverages> carrierCoveragesCollection) {
        this.carrierCoveragesCollection = carrierCoveragesCollection;
    }

    public Banks getBankId() {
        return bankId;
    }

    public void setBankId(Banks bankId) {
        this.bankId = bankId;
    }

    @XmlTransient
    public Collection<Accounts> getAccountsCollection() {
        return accountsCollection;
    }

    public void setAccountsCollection(Collection<Accounts> accountsCollection) {
        this.accountsCollection = accountsCollection;
    }

    @XmlTransient
    public Collection<Drivers> getDriversCollection() {
        return driversCollection;
    }

    public void setDriversCollection(Collection<Drivers> driversCollection) {
        this.driversCollection = driversCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carriers)) {
            return false;
        }
        Carriers other = (Carriers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Carriers[ id=" + id + " ]";
    }
    
}
