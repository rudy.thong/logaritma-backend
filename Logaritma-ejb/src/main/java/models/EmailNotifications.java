/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "email_notifications")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmailNotifications.findAll", query = "SELECT e FROM EmailNotifications e"),
    @NamedQuery(name = "EmailNotifications.findById", query = "SELECT e FROM EmailNotifications e WHERE e.id = :id"),
    @NamedQuery(name = "EmailNotifications.findBySubject", query = "SELECT e FROM EmailNotifications e WHERE e.subject = :subject"),
    @NamedQuery(name = "EmailNotifications.findByContentPath", query = "SELECT e FROM EmailNotifications e WHERE e.contentPath = :contentPath"),
    @NamedQuery(name = "EmailNotifications.findByEmailTo", query = "SELECT e FROM EmailNotifications e WHERE e.emailTo = :emailTo"),
    @NamedQuery(name = "EmailNotifications.findByStatus", query = "SELECT e FROM EmailNotifications e WHERE e.status = :status"),
    @NamedQuery(name = "EmailNotifications.findByLastUpdated", query = "SELECT e FROM EmailNotifications e WHERE e.lastUpdated = :lastUpdated")})
public class EmailNotifications implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 256)
    @Column(name = "subject")
    private String subject;
    @Size(max = 256)
    @Column(name = "content_path")
    private String contentPath;
    @Size(max = 50)
    @Column(name = "email_to")
    private String emailTo;
    @Column(name = "status")
    private Character status;
    @Column(name = "last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    public EmailNotifications() {
    }

    public EmailNotifications(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContentPath() {
        return contentPath;
    }

    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }

    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmailNotifications)) {
            return false;
        }
        EmailNotifications other = (EmailNotifications) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.EmailNotifications[ id=" + id + " ]";
    }
    
}
