/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "orders")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orders.findAll", query = "SELECT o FROM Orders o"),
    @NamedQuery(name = "Orders.findById", query = "SELECT o FROM Orders o WHERE o.id = :id"),
    @NamedQuery(name = "Orders.findByTotalPrice", query = "SELECT o FROM Orders o WHERE o.totalPrice = :totalPrice"),
    @NamedQuery(name = "Orders.findByPickupEstimationEarliestDate", query = "SELECT o FROM Orders o WHERE o.pickupEstimationEarliestDate = :pickupEstimationEarliestDate"),
    @NamedQuery(name = "Orders.findByPickupEstimationLatestDate", query = "SELECT o FROM Orders o WHERE o.pickupEstimationLatestDate = :pickupEstimationLatestDate"),
    @NamedQuery(name = "Orders.findByPickupActualDate", query = "SELECT o FROM Orders o WHERE o.pickupActualDate = :pickupActualDate"),
    @NamedQuery(name = "Orders.findByDeliveryEstimationEarliestDate", query = "SELECT o FROM Orders o WHERE o.deliveryEstimationEarliestDate = :deliveryEstimationEarliestDate"),
    @NamedQuery(name = "Orders.findByDeliveryEstimationLatestDate", query = "SELECT o FROM Orders o WHERE o.deliveryEstimationLatestDate = :deliveryEstimationLatestDate"),
    @NamedQuery(name = "Orders.findByDeliveryActualDate", query = "SELECT o FROM Orders o WHERE o.deliveryActualDate = :deliveryActualDate"),
    @NamedQuery(name = "Orders.findBySealNumber", query = "SELECT o FROM Orders o WHERE o.sealNumber = :sealNumber"),
    @NamedQuery(name = "Orders.findBySealColor", query = "SELECT o FROM Orders o WHERE o.sealColor = :sealColor"),
    @NamedQuery(name = "Orders.findBySealInputTime", query = "SELECT o FROM Orders o WHERE o.sealInputTime = :sealInputTime"),
    @NamedQuery(name = "Orders.findByPaymentDate", query = "SELECT o FROM Orders o WHERE o.paymentDate = :paymentDate"),
    @NamedQuery(name = "Orders.findBySettlementAmount", query = "SELECT o FROM Orders o WHERE o.settlementAmount = :settlementAmount"),
    @NamedQuery(name = "Orders.findBySettlementDate", query = "SELECT o FROM Orders o WHERE o.settlementDate = :settlementDate"),
    @NamedQuery(name = "Orders.findByAdditionalNotes", query = "SELECT o FROM Orders o WHERE o.additionalNotes = :additionalNotes"),
    @NamedQuery(name = "Orders.findByPenaltyCost", query = "SELECT o FROM Orders o WHERE o.penaltyCost = :penaltyCost"),
    @NamedQuery(name = "Orders.findByVehicleRating", query = "SELECT o FROM Orders o WHERE o.vehicleRating = :vehicleRating"),
    @NamedQuery(name = "Orders.findByDriverRating", query = "SELECT o FROM Orders o WHERE o.driverRating = :driverRating"),
    @NamedQuery(name = "Orders.findByShipperRating", query = "SELECT o FROM Orders o WHERE o.shipperRating = :shipperRating"),
    @NamedQuery(name = "Orders.findByStatus", query = "SELECT o FROM Orders o WHERE o.status = :status")})
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "total_price")
    private BigDecimal totalPrice;
    @Column(name = "pickup_estimation_earliest_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pickupEstimationEarliestDate;
    @Column(name = "pickup_estimation_latest_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pickupEstimationLatestDate;
    @Column(name = "pickup_actual_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pickupActualDate;
    @Column(name = "delivery_estimation_earliest_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveryEstimationEarliestDate;
    @Column(name = "delivery_estimation_latest_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveryEstimationLatestDate;
    @Column(name = "delivery_actual_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveryActualDate;
    @Size(max = 50)
    @Column(name = "seal_number")
    private String sealNumber;
    @Size(max = 100)
    @Column(name = "seal_color")
    private String sealColor;
    @Column(name = "seal_input_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sealInputTime;
    @Column(name = "payment_date")
    @Temporal(TemporalType.DATE)
    private Date paymentDate;
    @Column(name = "settlement_amount")
    private BigDecimal settlementAmount;
    @Column(name = "settlement_date")
    @Temporal(TemporalType.DATE)
    private Date settlementDate;
    @Size(max = 2147483647)
    @Column(name = "additional_notes")
    private String additionalNotes;
    @Column(name = "penalty_cost")
    private BigDecimal penaltyCost;
    @Column(name = "vehicle_rating")
    private Short vehicleRating;
    @Column(name = "driver_rating")
    private Short driverRating;
    @Column(name = "shipper_rating")
    private Short shipperRating;
    @Column(name = "status")
    private Character status;
    @OneToMany(mappedBy = "orderId")
    private Collection<OrderAccessories> orderAccessoriesCollection;
    @JoinColumn(name = "accessory_id", referencedColumnName = "id")
    @ManyToOne
    private Accessories accessoryId;
    @JoinColumn(name = "pickup_address_id", referencedColumnName = "id")
    @ManyToOne
    private Addresses pickupAddressId;
    @JoinColumn(name = "delivery_address_id", referencedColumnName = "id")
    @ManyToOne
    private Addresses deliveryAddressId;
    @JoinColumn(name = "destination_city_id", referencedColumnName = "id")
    @ManyToOne
    private Cities destinationCityId;
    @JoinColumn(name = "origin_city_id", referencedColumnName = "id")
    @ManyToOne
    private Cities originCityId;
    @JoinColumn(name = "job_id", referencedColumnName = "id")
    @ManyToOne
    private Jobs jobId;
    @JoinColumn(name = "payment_method_id", referencedColumnName = "id")
    @ManyToOne
    private PaymentMethods paymentMethodId;
    @JoinColumn(name = "payment_type_id", referencedColumnName = "id")
    @ManyToOne
    private PaymentTypes paymentTypeId;
    @JoinColumn(name = "shipper_contact_id", referencedColumnName = "id")
    @ManyToOne
    private ShipperContacts shipperContactId;
    @JoinColumn(name = "shipper_id", referencedColumnName = "id")
    @ManyToOne
    private Shippers shipperId;

    public Orders() {
    }

    public Orders(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getPickupEstimationEarliestDate() {
        return pickupEstimationEarliestDate;
    }

    public void setPickupEstimationEarliestDate(Date pickupEstimationEarliestDate) {
        this.pickupEstimationEarliestDate = pickupEstimationEarliestDate;
    }

    public Date getPickupEstimationLatestDate() {
        return pickupEstimationLatestDate;
    }

    public void setPickupEstimationLatestDate(Date pickupEstimationLatestDate) {
        this.pickupEstimationLatestDate = pickupEstimationLatestDate;
    }

    public Date getPickupActualDate() {
        return pickupActualDate;
    }

    public void setPickupActualDate(Date pickupActualDate) {
        this.pickupActualDate = pickupActualDate;
    }

    public Date getDeliveryEstimationEarliestDate() {
        return deliveryEstimationEarliestDate;
    }

    public void setDeliveryEstimationEarliestDate(Date deliveryEstimationEarliestDate) {
        this.deliveryEstimationEarliestDate = deliveryEstimationEarliestDate;
    }

    public Date getDeliveryEstimationLatestDate() {
        return deliveryEstimationLatestDate;
    }

    public void setDeliveryEstimationLatestDate(Date deliveryEstimationLatestDate) {
        this.deliveryEstimationLatestDate = deliveryEstimationLatestDate;
    }

    public Date getDeliveryActualDate() {
        return deliveryActualDate;
    }

    public void setDeliveryActualDate(Date deliveryActualDate) {
        this.deliveryActualDate = deliveryActualDate;
    }

    public String getSealNumber() {
        return sealNumber;
    }

    public void setSealNumber(String sealNumber) {
        this.sealNumber = sealNumber;
    }

    public String getSealColor() {
        return sealColor;
    }

    public void setSealColor(String sealColor) {
        this.sealColor = sealColor;
    }

    public Date getSealInputTime() {
        return sealInputTime;
    }

    public void setSealInputTime(Date sealInputTime) {
        this.sealInputTime = sealInputTime;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public BigDecimal getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(BigDecimal settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public Date getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getAdditionalNotes() {
        return additionalNotes;
    }

    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    public BigDecimal getPenaltyCost() {
        return penaltyCost;
    }

    public void setPenaltyCost(BigDecimal penaltyCost) {
        this.penaltyCost = penaltyCost;
    }

    public Short getVehicleRating() {
        return vehicleRating;
    }

    public void setVehicleRating(Short vehicleRating) {
        this.vehicleRating = vehicleRating;
    }

    public Short getDriverRating() {
        return driverRating;
    }

    public void setDriverRating(Short driverRating) {
        this.driverRating = driverRating;
    }

    public Short getShipperRating() {
        return shipperRating;
    }

    public void setShipperRating(Short shipperRating) {
        this.shipperRating = shipperRating;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<OrderAccessories> getOrderAccessoriesCollection() {
        return orderAccessoriesCollection;
    }

    public void setOrderAccessoriesCollection(Collection<OrderAccessories> orderAccessoriesCollection) {
        this.orderAccessoriesCollection = orderAccessoriesCollection;
    }

    public Accessories getAccessoryId() {
        return accessoryId;
    }

    public void setAccessoryId(Accessories accessoryId) {
        this.accessoryId = accessoryId;
    }

    public Addresses getPickupAddressId() {
        return pickupAddressId;
    }

    public void setPickupAddressId(Addresses pickupAddressId) {
        this.pickupAddressId = pickupAddressId;
    }

    public Addresses getDeliveryAddressId() {
        return deliveryAddressId;
    }

    public void setDeliveryAddressId(Addresses deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    public Cities getDestinationCityId() {
        return destinationCityId;
    }

    public void setDestinationCityId(Cities destinationCityId) {
        this.destinationCityId = destinationCityId;
    }

    public Cities getOriginCityId() {
        return originCityId;
    }

    public void setOriginCityId(Cities originCityId) {
        this.originCityId = originCityId;
    }

    public Jobs getJobId() {
        return jobId;
    }

    public void setJobId(Jobs jobId) {
        this.jobId = jobId;
    }

    public PaymentMethods getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(PaymentMethods paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public PaymentTypes getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(PaymentTypes paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public ShipperContacts getShipperContactId() {
        return shipperContactId;
    }

    public void setShipperContactId(ShipperContacts shipperContactId) {
        this.shipperContactId = shipperContactId;
    }

    public Shippers getShipperId() {
        return shipperId;
    }

    public void setShipperId(Shippers shipperId) {
        this.shipperId = shipperId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orders)) {
            return false;
        }
        Orders other = (Orders) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Orders[ id=" + id + " ]";
    }
    
}
