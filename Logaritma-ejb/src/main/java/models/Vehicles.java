/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "vehicles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vehicles.findAll", query = "SELECT v FROM Vehicles v"),
    @NamedQuery(name = "Vehicles.findById", query = "SELECT v FROM Vehicles v WHERE v.id = :id"),
    @NamedQuery(name = "Vehicles.findByLicensePlate", query = "SELECT v FROM Vehicles v WHERE v.licensePlate = :licensePlate"),
    @NamedQuery(name = "Vehicles.findByDocStnkPath", query = "SELECT v FROM Vehicles v WHERE v.docStnkPath = :docStnkPath"),
    @NamedQuery(name = "Vehicles.findByDocKirPath", query = "SELECT v FROM Vehicles v WHERE v.docKirPath = :docKirPath"),
    @NamedQuery(name = "Vehicles.findByDocTrayekPath", query = "SELECT v FROM Vehicles v WHERE v.docTrayekPath = :docTrayekPath"),
    @NamedQuery(name = "Vehicles.findByStatus", query = "SELECT v FROM Vehicles v WHERE v.status = :status"),
    @NamedQuery(name = "Vehicles.findByAverageRating", query = "SELECT v FROM Vehicles v WHERE v.averageRating = :averageRating")})
public class Vehicles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 10)
    @Column(name = "license_plate")
    private String licensePlate;
    @Size(max = 255)
    @Column(name = "doc_stnk_path")
    private String docStnkPath;
    @Size(max = 255)
    @Column(name = "doc_kir_path")
    private String docKirPath;
    @Size(max = 255)
    @Column(name = "doc_trayek_path")
    private String docTrayekPath;
    @Column(name = "status")
    private Character status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "average_rating")
    private BigDecimal averageRating;
    @JoinColumn(name = "carrier_id", referencedColumnName = "id")
    @ManyToOne
    private Carriers carrierId;
    @JoinColumn(name = "item_category_id", referencedColumnName = "id")
    @ManyToOne
    private ItemCategories itemCategoryId;
    @JoinColumn(name = "vehicle_category_id", referencedColumnName = "id")
    @ManyToOne
    private VehicleCategories vehicleCategoryId;
    @OneToMany(mappedBy = "vehicleId")
    private Collection<VehicleAccessories> vehicleAccessoriesCollection;
    @OneToMany(mappedBy = "vehicleId")
    private Collection<Jobs> jobsCollection;

    public Vehicles() {
    }

    public Vehicles(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getDocStnkPath() {
        return docStnkPath;
    }

    public void setDocStnkPath(String docStnkPath) {
        this.docStnkPath = docStnkPath;
    }

    public String getDocKirPath() {
        return docKirPath;
    }

    public void setDocKirPath(String docKirPath) {
        this.docKirPath = docKirPath;
    }

    public String getDocTrayekPath() {
        return docTrayekPath;
    }

    public void setDocTrayekPath(String docTrayekPath) {
        this.docTrayekPath = docTrayekPath;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public BigDecimal getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(BigDecimal averageRating) {
        this.averageRating = averageRating;
    }

    public Carriers getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(Carriers carrierId) {
        this.carrierId = carrierId;
    }

    public ItemCategories getItemCategoryId() {
        return itemCategoryId;
    }

    public void setItemCategoryId(ItemCategories itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }

    public VehicleCategories getVehicleCategoryId() {
        return vehicleCategoryId;
    }

    public void setVehicleCategoryId(VehicleCategories vehicleCategoryId) {
        this.vehicleCategoryId = vehicleCategoryId;
    }

    @XmlTransient
    public Collection<VehicleAccessories> getVehicleAccessoriesCollection() {
        return vehicleAccessoriesCollection;
    }

    public void setVehicleAccessoriesCollection(Collection<VehicleAccessories> vehicleAccessoriesCollection) {
        this.vehicleAccessoriesCollection = vehicleAccessoriesCollection;
    }

    @XmlTransient
    public Collection<Jobs> getJobsCollection() {
        return jobsCollection;
    }

    public void setJobsCollection(Collection<Jobs> jobsCollection) {
        this.jobsCollection = jobsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vehicles)) {
            return false;
        }
        Vehicles other = (Vehicles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Vehicles[ id=" + id + " ]";
    }
    
}
