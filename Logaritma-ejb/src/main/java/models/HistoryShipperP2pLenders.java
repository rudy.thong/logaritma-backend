/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "history_shipper_p2p_lenders")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistoryShipperP2pLenders.findAll", query = "SELECT h FROM HistoryShipperP2pLenders h"),
    @NamedQuery(name = "HistoryShipperP2pLenders.findById", query = "SELECT h FROM HistoryShipperP2pLenders h WHERE h.id = :id"),
    @NamedQuery(name = "HistoryShipperP2pLenders.findByStatus", query = "SELECT h FROM HistoryShipperP2pLenders h WHERE h.status = :status")})
public class HistoryShipperP2pLenders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "status")
    private Character status;
    @JoinColumn(name = "p2p_lender_id", referencedColumnName = "id")
    @ManyToOne
    private P2pLenders p2pLenderId;
    @JoinColumn(name = "shipper_id", referencedColumnName = "id")
    @ManyToOne
    private Shippers shipperId;

    public HistoryShipperP2pLenders() {
    }

    public HistoryShipperP2pLenders(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public P2pLenders getP2pLenderId() {
        return p2pLenderId;
    }

    public void setP2pLenderId(P2pLenders p2pLenderId) {
        this.p2pLenderId = p2pLenderId;
    }

    public Shippers getShipperId() {
        return shipperId;
    }

    public void setShipperId(Shippers shipperId) {
        this.shipperId = shipperId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoryShipperP2pLenders)) {
            return false;
        }
        HistoryShipperP2pLenders other = (HistoryShipperP2pLenders) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.HistoryShipperP2pLenders[ id=" + id + " ]";
    }
    
}
