/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "accessories")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Accessories.findAll", query = "SELECT a FROM Accessories a"),
    @NamedQuery(name = "Accessories.findById", query = "SELECT a FROM Accessories a WHERE a.id = :id"),
    @NamedQuery(name = "Accessories.findByName", query = "SELECT a FROM Accessories a WHERE a.name = :name"),
    @NamedQuery(name = "Accessories.findByAdditionalPricePlus", query = "SELECT a FROM Accessories a WHERE a.additionalPricePlus = :additionalPricePlus")})
public class Accessories implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "additional_price_plus")
    private BigDecimal additionalPricePlus;
    @OneToMany(mappedBy = "accessoryId")
    private Collection<OrderAccessories> orderAccessoriesCollection;
    @OneToMany(mappedBy = "accessoryId")
    private Collection<VehicleAccessories> vehicleAccessoriesCollection;
    @OneToMany(mappedBy = "accessoryId")
    private Collection<Orders> ordersCollection;

    public Accessories() {
    }

    public Accessories(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAdditionalPricePlus() {
        return additionalPricePlus;
    }

    public void setAdditionalPricePlus(BigDecimal additionalPricePlus) {
        this.additionalPricePlus = additionalPricePlus;
    }

    @XmlTransient
    public Collection<OrderAccessories> getOrderAccessoriesCollection() {
        return orderAccessoriesCollection;
    }

    public void setOrderAccessoriesCollection(Collection<OrderAccessories> orderAccessoriesCollection) {
        this.orderAccessoriesCollection = orderAccessoriesCollection;
    }

    @XmlTransient
    public Collection<VehicleAccessories> getVehicleAccessoriesCollection() {
        return vehicleAccessoriesCollection;
    }

    public void setVehicleAccessoriesCollection(Collection<VehicleAccessories> vehicleAccessoriesCollection) {
        this.vehicleAccessoriesCollection = vehicleAccessoriesCollection;
    }

    @XmlTransient
    public Collection<Orders> getOrdersCollection() {
        return ordersCollection;
    }

    public void setOrdersCollection(Collection<Orders> ordersCollection) {
        this.ordersCollection = ordersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Accessories)) {
            return false;
        }
        Accessories other = (Accessories) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Accessories[ id=" + id + " ]";
    }
    
}
