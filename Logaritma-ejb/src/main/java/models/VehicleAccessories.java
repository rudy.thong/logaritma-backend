/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "vehicle_accessories")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VehicleAccessories.findAll", query = "SELECT v FROM VehicleAccessories v"),
    @NamedQuery(name = "VehicleAccessories.findById", query = "SELECT v FROM VehicleAccessories v WHERE v.id = :id")})
public class VehicleAccessories implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @JoinColumn(name = "accessory_id", referencedColumnName = "id")
    @ManyToOne
    private Accessories accessoryId;
    @JoinColumn(name = "vehicle_id", referencedColumnName = "id")
    @ManyToOne
    private Vehicles vehicleId;

    public VehicleAccessories() {
    }

    public VehicleAccessories(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Accessories getAccessoryId() {
        return accessoryId;
    }

    public void setAccessoryId(Accessories accessoryId) {
        this.accessoryId = accessoryId;
    }

    public Vehicles getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Vehicles vehicleId) {
        this.vehicleId = vehicleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleAccessories)) {
            return false;
        }
        VehicleAccessories other = (VehicleAccessories) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.VehicleAccessories[ id=" + id + " ]";
    }
    
}
