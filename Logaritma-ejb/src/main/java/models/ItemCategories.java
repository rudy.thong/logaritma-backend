/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "item_categories")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemCategories.findAll", query = "SELECT i FROM ItemCategories i"),
    @NamedQuery(name = "ItemCategories.findById", query = "SELECT i FROM ItemCategories i WHERE i.id = :id"),
    @NamedQuery(name = "ItemCategories.findByName", query = "SELECT i FROM ItemCategories i WHERE i.name = :name"),
    @NamedQuery(name = "ItemCategories.findByExamples", query = "SELECT i FROM ItemCategories i WHERE i.examples = :examples"),
    @NamedQuery(name = "ItemCategories.findByAdditionalPriceMultiply", query = "SELECT i FROM ItemCategories i WHERE i.additionalPriceMultiply = :additionalPriceMultiply")})
public class ItemCategories implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    @Size(max = 2147483647)
    @Column(name = "examples")
    private String examples;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "additional_price_multiply")
    private BigDecimal additionalPriceMultiply;
    @OneToMany(mappedBy = "itemCategoryId")
    private Collection<Vehicles> vehiclesCollection;

    public ItemCategories() {
    }

    public ItemCategories(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExamples() {
        return examples;
    }

    public void setExamples(String examples) {
        this.examples = examples;
    }

    public BigDecimal getAdditionalPriceMultiply() {
        return additionalPriceMultiply;
    }

    public void setAdditionalPriceMultiply(BigDecimal additionalPriceMultiply) {
        this.additionalPriceMultiply = additionalPriceMultiply;
    }

    @XmlTransient
    public Collection<Vehicles> getVehiclesCollection() {
        return vehiclesCollection;
    }

    public void setVehiclesCollection(Collection<Vehicles> vehiclesCollection) {
        this.vehiclesCollection = vehiclesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemCategories)) {
            return false;
        }
        ItemCategories other = (ItemCategories) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.ItemCategories[ id=" + id + " ]";
    }
    
}
