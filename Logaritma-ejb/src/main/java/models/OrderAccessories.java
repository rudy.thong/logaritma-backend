/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "order_accessories")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderAccessories.findAll", query = "SELECT o FROM OrderAccessories o"),
    @NamedQuery(name = "OrderAccessories.findById", query = "SELECT o FROM OrderAccessories o WHERE o.id = :id"),
    @NamedQuery(name = "OrderAccessories.findByIsCompleted", query = "SELECT o FROM OrderAccessories o WHERE o.isCompleted = :isCompleted")})
public class OrderAccessories implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "is_completed")
    private Short isCompleted;
    @JoinColumn(name = "accessory_id", referencedColumnName = "id")
    @ManyToOne
    private Accessories accessoryId;
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    @ManyToOne
    private Orders orderId;

    public OrderAccessories() {
    }

    public OrderAccessories(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getIsCompleted() {
        return isCompleted;
    }

    public void setIsCompleted(Short isCompleted) {
        this.isCompleted = isCompleted;
    }

    public Accessories getAccessoryId() {
        return accessoryId;
    }

    public void setAccessoryId(Accessories accessoryId) {
        this.accessoryId = accessoryId;
    }

    public Orders getOrderId() {
        return orderId;
    }

    public void setOrderId(Orders orderId) {
        this.orderId = orderId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderAccessories)) {
            return false;
        }
        OrderAccessories other = (OrderAccessories) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.OrderAccessories[ id=" + id + " ]";
    }
    
}
