/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Herianto
 */
@Entity
@Table(name = "p2p_lenders")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "P2pLenders.findAll", query = "SELECT p FROM P2pLenders p"),
    @NamedQuery(name = "P2pLenders.findById", query = "SELECT p FROM P2pLenders p WHERE p.id = :id"),
    @NamedQuery(name = "P2pLenders.findByName", query = "SELECT p FROM P2pLenders p WHERE p.name = :name")})
public class P2pLenders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "p2pLenderId")
    private Collection<Shippers> shippersCollection;
    @OneToMany(mappedBy = "p2pLenderId")
    private Collection<HistoryShipperP2pLenders> historyShipperP2pLendersCollection;

    public P2pLenders() {
    }

    public P2pLenders(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Shippers> getShippersCollection() {
        return shippersCollection;
    }

    public void setShippersCollection(Collection<Shippers> shippersCollection) {
        this.shippersCollection = shippersCollection;
    }

    @XmlTransient
    public Collection<HistoryShipperP2pLenders> getHistoryShipperP2pLendersCollection() {
        return historyShipperP2pLendersCollection;
    }

    public void setHistoryShipperP2pLendersCollection(Collection<HistoryShipperP2pLenders> historyShipperP2pLendersCollection) {
        this.historyShipperP2pLendersCollection = historyShipperP2pLendersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof P2pLenders)) {
            return false;
        }
        P2pLenders other = (P2pLenders) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.P2pLenders[ id=" + id + " ]";
    }
    
}
