/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.Banks;

/**
 *
 * @author Herianto
 */
@Local
public interface BanksFacadeLocal {

    void create(Banks banks);

    void edit(Banks banks);

    void remove(Banks banks);

    Banks find(Object id);

    List<Banks> findAll();

    List<Banks> findRange(int[] range);

    int count();
    
}
