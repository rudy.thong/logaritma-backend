/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.Carriers;

/**
 *
 * @author Herianto
 */
@Local
public interface CarriersFacadeLocal {

    void create(Carriers carriers);

    void edit(Carriers carriers);

    void remove(Carriers carriers);

    Carriers find(Object id);

    List<Carriers> findAll();

    List<Carriers> findRange(int[] range);

    int count();
        
}
