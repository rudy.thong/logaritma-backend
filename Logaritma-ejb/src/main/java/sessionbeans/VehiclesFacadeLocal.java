/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.Vehicles;

/**
 *
 * @author Herianto
 */
@Local
public interface VehiclesFacadeLocal {

    void create(Vehicles vehicles);

    void edit(Vehicles vehicles);

    void remove(Vehicles vehicles);

    Vehicles find(Object id);

    List<Vehicles> findAll();

    List<Vehicles> findRange(int[] range);

    int count();
    
}
