/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.VehicleCategories;

/**
 *
 * @author Herianto
 */
@Local
public interface VehicleCategoriesFacadeLocal {

    void create(VehicleCategories vehicleCategories);

    void edit(VehicleCategories vehicleCategories);

    void remove(VehicleCategories vehicleCategories);

    VehicleCategories find(Object id);

    List<VehicleCategories> findAll();

    List<VehicleCategories> findRange(int[] range);

    int count();
    
}
