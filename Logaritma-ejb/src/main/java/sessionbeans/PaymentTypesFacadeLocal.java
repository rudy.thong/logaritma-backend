/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.PaymentTypes;

/**
 *
 * @author Herianto
 */
@Local
public interface PaymentTypesFacadeLocal {

    void create(PaymentTypes paymentTypes);

    void edit(PaymentTypes paymentTypes);

    void remove(PaymentTypes paymentTypes);

    PaymentTypes find(Object id);

    List<PaymentTypes> findAll();

    List<PaymentTypes> findRange(int[] range);

    int count();
    
}
