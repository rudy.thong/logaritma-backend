/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import models.Accounts;
import models.Orders;
import models.Shippers;

/**
 *
 * @author Herianto
 */
@Stateless
public class OrdersFacade extends AbstractFacade<Orders> implements OrdersFacadeLocal {

    @PersistenceContext(unitName = "core_Logaritma-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrdersFacade() {
        super(Orders.class);
    }
    
    @Override
    public int getTotalJobsByShipperIdandStatus(Shippers shipperId, char status){
        int total = 0;
        try {
            return (int) getEntityManager().createQuery("select count(o) from Orders o where o.shipperId = :shipperId and o.jobId.status = :status")
                    .setParameter("shipperId", shipperId)
                    .setParameter("status", status)
                    .getSingleResult();                        
        } catch (NoResultException | NonUniqueResultException nre) {
            return total;
        }
    }
}
