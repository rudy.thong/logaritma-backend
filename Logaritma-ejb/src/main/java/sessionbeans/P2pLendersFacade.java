/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import models.P2pLenders;

/**
 *
 * @author Herianto
 */
@Stateless
public class P2pLendersFacade extends AbstractFacade<P2pLenders> implements P2pLendersFacadeLocal {

    @PersistenceContext(unitName = "core_Logaritma-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public P2pLendersFacade() {
        super(P2pLenders.class);
    }
    
}
