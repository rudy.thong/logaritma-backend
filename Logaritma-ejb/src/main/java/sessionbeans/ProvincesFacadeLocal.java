/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.Provinces;

/**
 *
 * @author Herianto
 */
@Local
public interface ProvincesFacadeLocal {

    void create(Provinces provinces);

    void edit(Provinces provinces);

    void remove(Provinces provinces);

    Provinces find(Object id);

    List<Provinces> findAll();

    List<Provinces> findRange(int[] range);

    int count();
    
}
