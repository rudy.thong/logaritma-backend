/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.HistoryShipperP2pLenders;

/**
 *
 * @author Herianto
 */
@Local
public interface HistoryShipperP2pLendersFacadeLocal {

    void create(HistoryShipperP2pLenders historyShipperP2pLenders);

    void edit(HistoryShipperP2pLenders historyShipperP2pLenders);

    void remove(HistoryShipperP2pLenders historyShipperP2pLenders);

    HistoryShipperP2pLenders find(Object id);

    List<HistoryShipperP2pLenders> findAll();

    List<HistoryShipperP2pLenders> findRange(int[] range);

    int count();
    
}
