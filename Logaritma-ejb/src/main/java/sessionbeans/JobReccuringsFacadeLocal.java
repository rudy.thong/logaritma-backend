/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.JobReccurings;

/**
 *
 * @author Herianto
 */
@Local
public interface JobReccuringsFacadeLocal {

    void create(JobReccurings jobReccurings);

    void edit(JobReccurings jobReccurings);

    void remove(JobReccurings jobReccurings);

    JobReccurings find(Object id);

    List<JobReccurings> findAll();

    List<JobReccurings> findRange(int[] range);

    int count();
    
}
