/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import models.PaymentTypes;

/**
 *
 * @author Herianto
 */
@Stateless
public class PaymentTypesFacade extends AbstractFacade<PaymentTypes> implements PaymentTypesFacadeLocal {

    @PersistenceContext(unitName = "core_Logaritma-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PaymentTypesFacade() {
        super(PaymentTypes.class);
    }
    
}
