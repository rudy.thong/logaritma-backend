/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.Config;

/**
 *
 * @author Herianto
 */
@Local
public interface ConfigFacadeLocal {

    void create(Config config);

    void edit(Config config);

    void remove(Config config);

    Config find(Object id);

    List<Config> findAll();

    List<Config> findRange(int[] range);

    int count();
    
}
