/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.CarrierCoverages;

/**
 *
 * @author Herianto
 */
@Local
public interface CarrierCoveragesFacadeLocal {

    void create(CarrierCoverages carrierCoverages);

    void edit(CarrierCoverages carrierCoverages);

    void remove(CarrierCoverages carrierCoverages);

    CarrierCoverages find(Object id);

    List<CarrierCoverages> findAll();

    List<CarrierCoverages> findRange(int[] range);

    int count();
    
}
