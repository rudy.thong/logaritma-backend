/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.P2pLenders;

/**
 *
 * @author Herianto
 */
@Local
public interface P2pLendersFacadeLocal {

    void create(P2pLenders p2pLenders);

    void edit(P2pLenders p2pLenders);

    void remove(P2pLenders p2pLenders);

    P2pLenders find(Object id);

    List<P2pLenders> findAll();

    List<P2pLenders> findRange(int[] range);

    int count();
    
}
