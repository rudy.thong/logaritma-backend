/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.Drivers;

/**
 *
 * @author Herianto
 */
@Local
public interface DriversFacadeLocal {

    void create(Drivers drivers);

    void edit(Drivers drivers);

    void remove(Drivers drivers);

    Drivers find(Object id);

    List<Drivers> findAll();

    List<Drivers> findRange(int[] range);

    int count();
    
}
