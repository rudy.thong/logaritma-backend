/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import models.Carriers;

/**
 *
 * @author Herianto
 */
@Stateless
public class CarriersFacade extends AbstractFacade<Carriers> implements CarriersFacadeLocal {

    @PersistenceContext(unitName = "core_Logaritma-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CarriersFacade() {
        super(Carriers.class);
    }
       
}
