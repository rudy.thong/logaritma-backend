/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.ShipperContacts;

/**
 *
 * @author Herianto
 */
@Local
public interface ShipperContactsFacadeLocal {

    void create(ShipperContacts shipperContacts);

    void edit(ShipperContacts shipperContacts);

    void remove(ShipperContacts shipperContacts);

    ShipperContacts find(Object id);

    List<ShipperContacts> findAll();

    List<ShipperContacts> findRange(int[] range);

    int count();
    
}
