/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.Accounts;
import models.Carriers;
import models.Shippers;

/**
 *
 * @author Herianto
 */
@Local
public interface AccountsFacadeLocal {

    void create(Accounts accounts);

    void edit(Accounts accounts);

    void remove(Accounts accounts);

    Accounts find(Object id);

    List<Accounts> findAll();

    List<Accounts> findRange(int[] range);

    int count();
    
    Accounts findByUsername(String username);
    
    Accounts findByLoginCarrierId(Carriers carrierId, String currentSession);
    
    Accounts findByLoginShipperId(Shippers shipperId, String currentSession);

    Accounts findByEmail(String email);    

}
