/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.OrderAccessories;

/**
 *
 * @author Herianto
 */
@Local
public interface OrderAccessoriesFacadeLocal {

    void create(OrderAccessories orderAccessories);

    void edit(OrderAccessories orderAccessories);

    void remove(OrderAccessories orderAccessories);

    OrderAccessories find(Object id);

    List<OrderAccessories> findAll();

    List<OrderAccessories> findRange(int[] range);

    int count();
    
}
