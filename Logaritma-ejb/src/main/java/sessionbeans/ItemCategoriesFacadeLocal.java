/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.ItemCategories;

/**
 *
 * @author Herianto
 */
@Local
public interface ItemCategoriesFacadeLocal {

    void create(ItemCategories itemCategories);

    void edit(ItemCategories itemCategories);

    void remove(ItemCategories itemCategories);

    ItemCategories find(Object id);

    List<ItemCategories> findAll();

    List<ItemCategories> findRange(int[] range);

    int count();
    
}
