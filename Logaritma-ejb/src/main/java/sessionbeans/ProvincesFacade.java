/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import models.Provinces;

/**
 *
 * @author Herianto
 */
@Stateless
public class ProvincesFacade extends AbstractFacade<Provinces> implements ProvincesFacadeLocal {

    @PersistenceContext(unitName = "core_Logaritma-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProvincesFacade() {
        super(Provinces.class);
    }
    
}
