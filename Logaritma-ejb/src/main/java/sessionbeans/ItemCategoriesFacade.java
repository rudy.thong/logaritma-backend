/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import models.ItemCategories;

/**
 *
 * @author Herianto
 */
@Stateless
public class ItemCategoriesFacade extends AbstractFacade<ItemCategories> implements ItemCategoriesFacadeLocal {

    @PersistenceContext(unitName = "core_Logaritma-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ItemCategoriesFacade() {
        super(ItemCategories.class);
    }
    
}
