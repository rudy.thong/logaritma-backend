/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.VehicleAccessories;

/**
 *
 * @author Herianto
 */
@Local
public interface VehicleAccessoriesFacadeLocal {

    void create(VehicleAccessories vehicleAccessories);

    void edit(VehicleAccessories vehicleAccessories);

    void remove(VehicleAccessories vehicleAccessories);

    VehicleAccessories find(Object id);

    List<VehicleAccessories> findAll();

    List<VehicleAccessories> findRange(int[] range);

    int count();
    
}
