/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import models.Accounts;
import models.Carriers;
import models.Shippers;

/**
 *
 * @author Herianto
 */
@Stateless
public class AccountsFacade extends AbstractFacade<Accounts> implements AccountsFacadeLocal {

    @PersistenceContext(unitName = "core_Logaritma-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AccountsFacade() {
        super(Accounts.class);
    }
    
    @Override
    public Accounts findByUsername(String username) {
        try {
            return (Accounts) getEntityManager().createQuery("SELECT a FROM Accounts a WHERE LOWER(a.username) = :username")
                    .setParameter("username", username.toLowerCase())
                    .getSingleResult();            
        } catch (NoResultException | NonUniqueResultException nre) {
            return null;
        }
    }

    @Override
    public Accounts findByEmail(String email) {
        try {
            return (Accounts) getEntityManager().createQuery("SELECT a FROM Accounts a WHERE LOWER(a.email) = :email")
                    .setParameter("email", email.toLowerCase())
                    .getSingleResult();            
        } catch (NoResultException | NonUniqueResultException nre) {
            return null;
        }
    }
    
    @Override
    public Accounts findByLoginCarrierId(Carriers carrierId, String currentSession) {
        try {
            return (Accounts) getEntityManager().createQuery("SELECT a FROM Accounts a WHERE a.carrierId = :carrierId AND a.currentSession = :currentSession")
                    .setParameter("carrierId", carrierId)
                    .setParameter("currentSession", currentSession)
                    .getSingleResult();                        
        } catch (NoResultException | NonUniqueResultException nre) {
            return null;
        }
    }
    
    @Override
    public Accounts findByLoginShipperId(Shippers shipperId, String currentSession) {
        try {
            return (Accounts) getEntityManager().createQuery("SELECT a FROM Accounts a WHERE a.shipperId= :shipperId AND a.currentSession = :currentSession")
                    .setParameter("shipperId", shipperId)
                    .setParameter("currentSession", currentSession)
                    .getSingleResult();                        
        } catch (NoResultException | NonUniqueResultException nre) {
            return null;
        }
    }
    
}
