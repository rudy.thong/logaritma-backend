/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbeans;

import java.util.List;
import javax.ejb.Local;
import models.EmailNotifications;

/**
 *
 * @author Herianto
 */
@Local
public interface EmailNotificationsFacadeLocal {

    void create(EmailNotifications emailNotifications);

    void edit(EmailNotifications emailNotifications);

    void remove(EmailNotifications emailNotifications);

    EmailNotifications find(Object id);

    List<EmailNotifications> findAll();

    List<EmailNotifications> findRange(int[] range);

    int count();
    
}
